#pragma once
#include <iostream>


class DynamicIntArray_
{
   int    * items_;
   size_t capacity_;

public:
  DynamicIntArray_(size_t size);
  ~DynamicIntArray_();

  size_t size();
  bool resize(size_t newSize);

  int get(int index);
  void set(int index, int value);
};