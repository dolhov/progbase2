#include "dynamic_int_array.hpp"


DynamicIntArray_::DynamicIntArray_(size_t size)
{
    DynamicIntArray_::items_ = static_cast<int *> (malloc(size * sizeof(int)));
    DynamicIntArray_::capacity_ = size;
}

DynamicIntArray_::~DynamicIntArray_()
{
    free(DynamicIntArray_::items_);
    DynamicIntArray_::capacity_ = 0;
}

size_t DynamicIntArray_::size()
{
    return DynamicIntArray_::capacity_;
}


bool DynamicIntArray_::resize(size_t newSize)
{
    int * pointer;
    pointer = static_cast<int *> (realloc(DynamicIntArray_::items_, newSize * sizeof(int)));
    DynamicIntArray_::capacity_ = newSize;
    if (pointer == nullptr)
    {
        return false;
    }
    else 
    {
        DynamicIntArray_::items_ = pointer;
        return true;
    }
}

int DynamicIntArray_::get(int index)
{
    return DynamicIntArray_::items_[index];
}

void DynamicIntArray_::set(int index, int value)
{
    DynamicIntArray_::items_[index] = value;
}
