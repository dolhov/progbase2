#include "dynamic_int_array.hpp"

int main()
{
    DynamicIntArray_ * array = new DynamicIntArray_(10);
    for (int i = 0; i < 10; i++)
    {
        array->set(i, 42);
    }
    for (int i = 0; i < array->size(); i++)
    {
        std::cout << array->get(i) << " ";
    }
    std::cout << '\n';
    array->resize(20);
    for (int i = 10; i < array->size(); i++)
    {
        array->set(i, 13);
    }
    for (int i = 0; i < array->size(); i++)
    {
        std::cout << array->get(i) << " ";;
    }
    std::cout << '\n';
    array->resize(15);
    for (int i = 0; i < array->size(); i++)
    {
        std::cout << array->get(i) << " ";;
    }
    std::cout << '\n';
    delete array;
}