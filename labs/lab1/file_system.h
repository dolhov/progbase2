#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "list.h"

bool open_file(std::string path, std::ifstream * fin)
{
    fin->exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try
    {
        fin->open(path);
    }
    catch(const std::ifstream::failure& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "ERROR: can't open file\n";
        return false;
    }
    return true;
}


double read_number_from(std::ifstream * fin)
{
    std::string str;
    double number;
    if (fin->is_open() == true && fin->eof() == false)
    {
        *fin >> number;
        return number;
    }
}

void read_to_list(List * list, std::ifstream * fin)
{
    while(fin->eof() == false)
    {
        list->insert(list->size(), read_number_from(fin));
    }
}

bool close_file(std::ifstream * fin)
{
    fin->exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try
    {
        fin->close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "ERROR: can't close file\n";
        return false;
    }
    return true;
}