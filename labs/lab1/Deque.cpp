#include "Deque.h"

Deque::Deque() : array_{10}
{
    array_;
    size_ = 0;
}

size_t Deque::size()
{
    return size_;
}

void Deque::push_back(double value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ * 2);
    }
    array_.set(size_, value);
    size_ += 1;
}
double Deque::pop_back()
{
    size_ -= 1;
    return array_.get(size_);
}
void Deque::push_front(double value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ * 2);
    }
    for (int i = size_ + 1; i > 1; i--)
    {
        array_.set(i, array_.get(i - 1));
    }
    array_.set(0, value);
    size_ += 1;
}
double Deque::pop_front()
{
    double tmp = array_.get(0);
    for (int i = 0; i < size_; i++)
    {
        array_.set(i, array_.get(i + 1));
    }
    size_ -= 1;
    return tmp;
}

bool Deque::empty()
{
    return (size_ == 0);
}

void Deque::print()
{
    for (int i = 0; i < size_; i++)
    {
        std::cout << array_.get(i) << " ";
    }
    std::cout << "\n";
}