cmake_minimum_required(VERSION 2.8.9)

file(GLOB SOURCES "*.cpp")
add_executable(a.out ${SOURCES})

#Set compiler flags
SET(CMAKE_C_FLAGS "-std=c11 -Wall -pedantic-errors -Werror -Wno-unused -g")