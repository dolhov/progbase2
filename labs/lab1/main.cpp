#include "dynamic_double_array.h"
#include "list.h"
#include "Deque.h"
#include "file_system.h"


int main(int argc, char * argv[])
{
                                                            // 1
    std::ifstream fin;
    if (open_file(argv[1], &fin) != true)
    {
        exit(1);
    }
    List my_list;
    read_to_list(&my_list, &fin);
    close_file(&fin);
    std::cout << "List: " << '\n';
    for (int i = 0; i < my_list.size(); i++)
    {
        std::cout << my_list.get(i) << " ";
    }
    std::cout << '\n';
                                                        //  3
    {
        int i = 0;
        int j = my_list.size() - 1;
        while (i < j)
        {
            if (my_list.get(i) < 0)
            {
                while (my_list.get(j) < 0)
                {
                    j--;
                }
                double tmp = my_list.get(i);
                my_list.set(i, my_list.get(j));
                my_list.set(j, tmp);
                j--;
            }
            i++;
        }
    }
    std::cout << "Modified list with negative numbers in tail: \n";
    for (int i = 0; i < my_list.size(); i++)
    {
        std::cout << my_list.get(i) << " ";
    }
    std::cout << '\n';
                                                        // 6
    Deque my_deque1;
    Deque my_deque2;
    for (int i = 1; i < my_list.size(); i += 2)
    {
        my_deque1.push_back(my_list.get(i));
    }
    for (int i = 0; i < my_list.size(); i += 2)
    {
        my_deque2.push_back(my_list.get(i));
    }
    std::cout << "First deque: \n";
    my_deque1.print();
    std::cout << "Second deque: \n";
    my_deque2.print();
                                                // 9
    List new_list;
    {
        int tmp_size = my_deque1.size();
        for (int i = 0; i < tmp_size; i++)
        {
            new_list.push_back(my_deque1.pop_back());
        }
        tmp_size = my_deque2.size();
        for (int i = 0; i < tmp_size; i++)
        {
            new_list.push_back(my_deque2.pop_front());
        }
    }
    std::cout << "Final list: \n";
    {
        for (int i = 0; i < my_list.size(); i++)
        {
            std::cout << new_list.get(i) << " ";
        }
    }
    std::cout << "\n";
}