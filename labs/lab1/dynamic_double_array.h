#pragma once
#include <iostream>


class DynamicDoubleArray_
{
   double    * items_;
   size_t capacity_;

public:
  DynamicDoubleArray_(size_t size);
  ~DynamicDoubleArray_();

  size_t size();
  bool resize(size_t newSize);

  double get(int index);
  void set(int index, double value);
};