#include "dynamic_double_array.h"


DynamicDoubleArray_::DynamicDoubleArray_(size_t size)
{
    DynamicDoubleArray_::items_ = new double[size];
    DynamicDoubleArray_::capacity_ = size;
}

DynamicDoubleArray_::~DynamicDoubleArray_()
{
    delete[] items_;
    DynamicDoubleArray_::capacity_ = 0;
}

size_t DynamicDoubleArray_::size()
{
    return DynamicDoubleArray_::capacity_;
}


bool DynamicDoubleArray_::resize(size_t newSize)
{
    double * dest = new double[newSize];
    std::copy(items_, items_ + capacity_, dest);
    delete[] items_;
    items_ = dest;
}

double DynamicDoubleArray_::get(int index)
{
    return DynamicDoubleArray_::items_[index];
}

void DynamicDoubleArray_::set(int index, double value)
{
    DynamicDoubleArray_::items_[index] = value;
}
