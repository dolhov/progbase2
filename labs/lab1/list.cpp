#include "list.h"


List::List() : array_{10}
{
    array_;
    size_ = 0;
}

size_t List::size() 
{
    return size_;
}

double List::get(int index) 
{
    double a = array_.get(index);
    return a;
}

void List::set(int index, double value) 
{
    array_.set(index, value);
}

void List::insert(int index, double value)
{
    if (size_ == array_.size() - 1)
    {
        array_.resize(size_ * 2);
    }
    for (int i = size_ + 1; i > index; i--)
    {
        array_.set(i, array_.get(i - 1));
    }
    array_.set(index, value);
    size_ += 1;
}

void List::remove_at(int index)
{
    for (int i = index; i < size_; i++)
    {
        array_.set(i, array_.get(i + 1));
    }
    size_ -= 1;
}



void List::push_back(double value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ * 2);
    }
    array_.set(size_, value);
    size_ += 1;
}

void List::remove(double value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value == array_.get(i))
        {
            remove_at(i);
            break;
        }
    }
}

int List::index_of(double value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value == array_.get(i))
        {
            return i;
        }
    }
}

bool List::contains(double value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value == array_.get(i))
        {
            return true;
        }
    }
    return false;
}

bool List::empty()
{
    if (size_ == 0)
    {
        return true;
    }
    return false;
}

void List::clear()
{
    size_ = 0;
}