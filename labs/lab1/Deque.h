#pragma once
#include "dynamic_double_array.h"

class Deque
{
   DynamicDoubleArray_ array_;
   size_t size_;
   
public:
   /* ctor + ~ctor */
   Deque();
   size_t size(); // return number of items

   void push_back(double value);
   double pop_back();
   void push_front(double value);
   double pop_front();

   bool empty();
   void print();
};
