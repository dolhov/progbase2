#include "FileStorage.h"
#include "cui.h"



int main()
{
    FileStorage storage{"../data/"};

    Cui cui{&storage};
    cui.show();
}