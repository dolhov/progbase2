#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <optional>

#include "tour_operators.h"
#include "file_system.h"



class FileStorage
{
   std::string  dir_name_;

   std::fstream tour_operators_file_;

   // tour operators
   std::vector<tour_operator> loadTourOperators();
   void saveTourOperators(const std::vector<tour_operator> & tour_operators);
   int getNewTourOperatorId();

 public:
   explicit FileStorage(const std::string & dir_name = "");

   void setName(const std::string & dir_name);
   std::string name() const;

   bool isOpen() const;
   bool open(); 
   void close();

   // tour operators
   std::vector<tour_operator> getAllTourOperators(void);
   std::optional<tour_operator> getTourOperatorById(int operator_id);
   bool updateTourOperator(const tour_operator &tour_op);
   bool removeTourOperator(int operator_id);
   int insertTourOperator(const tour_operator &tour_operator);
};
