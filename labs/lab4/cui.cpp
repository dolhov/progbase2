#include "cui.h"


void Cui::show()
{
    std::cout << "To choose option write it's number;\n";
    std::cout << "Options:\n1)Operators Info\n2)Operator info\n3)Change field of operator\n4)Delete operator\n5)Add new operator\n6)Show options again\n7)Exit\n";
    int option = 0;
    
    while(1)
    {
        if (std::cin.fail())
        {
            // get rid of failure state
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        std::cin >> option;
        if (option == 1)
        {
            std::cout << "Operators menu:\n";
            this->operatorsMainMenu();
        }
        else if (option == 2)
        {
            std::cout << "Enter id of operator:\n";
            int id = 0;
            std::cin >> id;
            operatorsMenu(id);
        }
        else if (option == 3)
        {
            std::cout << "Enter id of operator:\n";
            int id = 0;
            std::cin >> id;
            operatorsUpdateMenu(id);
            std::cout << "Field changed\n";
        }
        else if (option == 4)
        {
            std::cout << "Enter id of operator:\n";
            int id = 0;
            std::cin >> id;
            operatorsDeleteMenu(id);
        }
        else if (option == 5)
        {
            operatorsCreateMenu();
            std::cout << "Operator created\n";
        }
        else if (option == 6)
        {
            std::cout << "Options:\n1)Operators Info\n2)Operator info\n3)Change field of operator\n4)Delete operator\n5)Add new operator\n6)Show options again\n7)Exit\n";
        }
        else if (option == 7)
        {
            std::cout << "End of program";
            break;
        }
        else
        {
            std::cout << "Unknown option. Enter 6 to see list of options again\n";
        }
    }
}


void Cui::operatorsMainMenu()
{
    FileStorage * stor = storage_;
    std::vector<tour_operator> ops = stor->getAllTourOperators();
    std::cout << "All tour operators: \n";
    for (tour_operator & it : ops)
    {
        std::cout << it.id << " " << it.name << " " << it.city << " " << it.vacation << " " << it.cost << " " << it.currency << "\n";
    }
}

void Cui::operatorsMenu(int entity_id)
{
    std::optional<tour_operator> op = storage_->getTourOperatorById(entity_id);
    if (op)
    {
        tour_operator tour = op.value();
        std::cout << tour.id << " " << tour.name << " " << tour.city << " " << tour.vacation << " " << tour.cost << " " << tour.currency << "\n";
    }
    else
    {
        std::cout << "Not found\n";
    }
    
}

void Cui::operatorsUpdateMenu(int entity_id)
{
    std::optional<tour_operator> op = storage_->getTourOperatorById(entity_id);
    tour_operator tour = op.value();
    if (op)
    {
        int option = 20;
        std::cout << "Chose field to change:\n1)Name\n2)City\n3)Vacation\n4)Cost\n5)Currency\n";
        std::cin >> option;
        std::string change;
        std::cout << "Write changes: " << std::endl;
        std::cin.get();
        std::getline(std::cin, change);
        if (option == 1)
        {
            tour.name = change;
            storage_->updateTourOperator(tour);
        }
        else if (option == 2)
        {
            tour.city = change;
            storage_->updateTourOperator(tour);
        }
        else if (option == 3)
        {
            tour.vacation = change;
            storage_->updateTourOperator(tour);
        }
        else if (option == 4)
        {
            tour.cost = std::stoi(change);
            storage_->updateTourOperator(tour);
        }
        else if (option == 5)
        {
            tour.currency = change;
            storage_->updateTourOperator(tour);
        }
        else
        {
            std::cout << "Unknown option";
        }
        
    }
    else
    {
        std::cout << "Not found\n";
    }
}


void Cui::operatorsDeleteMenu(int entity_id)
{
    storage_->removeTourOperator(entity_id);
}

void Cui::operatorsCreateMenu()
{
    tour_operator op;
    std::string change;
    std::cout << "Enter operator's name: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::cin.get();
    std::getline(std::cin, change);
    op.name = change;
    std::cout << "Enter operator's city: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::cin.get();
    std::getline(std::cin, change);
    op.city = change;
    std::cout << "Enter operator's vacation: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::cin.get();
    std::getline(std::cin, change);
    op.vacation = change;
    std::cout << "Enter operator's cost: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::cin.get();
    std::getline(std::cin, change);
    op.cost = std::stoi(change);
    std::cout << "Enter operator's currency: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::cin.get();
    std::getline(std::cin, change);
    op.currency = change;
    storage_->insertTourOperator(op);
}