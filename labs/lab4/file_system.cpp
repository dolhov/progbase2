#include "file_system.h"


std::string read_csv_line_from(std::fstream & fin)
{
    std::string my_data;
    std::string super_string;
    while (std::getline(fin, my_data))
    {
        if (!my_data.empty())
        {
            super_string += my_data + '\n';
        }
        if (fin.eof())
        {
            break;
        }
    }
    return super_string;
}




std::vector<tour_operator> createEntityListFromTable(StringTable & csvTable)
{
    std::vector<tour_operator> my_list;
    tour_operator tour;
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        tour.id = std::stoi(csvTable.at(i, 0));
        tour.name = csvTable.at(i, 1);
        tour.city = csvTable.at(i, 2);
        tour.vacation = csvTable.at(i, 3);
        tour.cost = std::stoi(csvTable.at(i, 4));
        tour.currency = csvTable.at(i, 5);
        my_list.push_back(tour);
    }
    return my_list;
}

StringTable createTableFromEntityList(std::vector<tour_operator> & list)
{
    StringTable my_table(list.size(), 6);
    //id,name,city,vacation,cost,currency
    for (int i = 0; i < list.size(); i++)
    {
        my_table.at(i, 0) = std::to_string(list[i].id);
        my_table.at(i, 1) = list[i].name;
        my_table.at(i, 2) = list[i].city;
        my_table.at(i, 3) = list[i].vacation;
        my_table.at(i, 4) = std::to_string(list[i].cost);
        my_table.at(i, 5) = list[i].currency;
    }
    return my_table;
}


void processEntities(std::vector<tour_operator> & items, std::string n) // Возможно, тут будут ошибки
{

    auto it = items.begin();
    auto it_end = items.end();
    for (; it != it_end; it++)
    {
        if (it->vacation == n)
        {
            items.erase(it);
        }
    }
}