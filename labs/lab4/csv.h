#pragma once
#include "string_table.h"


// parse csv, init and fill string table
StringTable Csv_parse(std::string &csvStr);


// create string and fill it with csv text 
std::string Csv_toString(StringTable &table);