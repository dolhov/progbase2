#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "tour_operators.h"
#include "csv.h"

std::string read_csv_line_from(std::fstream & fin);
std::vector<tour_operator> createEntityListFromTable(StringTable & csvTable);
StringTable createTableFromEntityList(std::vector<tour_operator> & list);
void processEntities(std::vector<tour_operator> & items, std::string n);
