#include "FileStorage.h"
#include "csvstorage.h"
#include "xmlstorage.h"
#include "cui.h"



int main()
{
    //CsvStorage storage{"../../../lab6/data/csv/"};
    XmlStorage xml_storage{"../../../lab6/data/xml/"};
    FileStorage * storage_ptr = &xml_storage;
    Cui cui{storage_ptr};
    cui.show();
}
