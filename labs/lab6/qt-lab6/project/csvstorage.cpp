#include "csvstorage.h"

std::vector<tour_operator> CsvStorage::loadTourOperators()
{
   tour_operators_file_.close();
   tour_operators_file_.open(directory_name() + "tour_operators.csv", std::ios_base::in);
   std::string useless = directory_name() + "tour_operators.csv";
   std::string csvtext = read_csv_line_from(tour_operators_file_);
   StringTable table = Csv_parse(csvtext);
   std::vector<tour_operator> ops = createEntityListFromTable(table);
   return ops;
}

void CsvStorage::saveTourOperators(const std::vector<tour_operator> & tour_operators)
{
   tour_operators_file_.close();
   tour_operators_file_.open(directory_name() + "tour_operators.csv", std::ios_base::out);
   std::vector<tour_operator> ops = tour_operators;
   StringTable table = createTableFromEntityList(ops);
   std::string csvtext = Csv_toString(table);
   tour_operators_file_ << csvtext;
   tour_operators_file_.flush();
}

int CsvStorage::getNewTourOperatorId()
{
   std::vector<tour_operator> ops = this->loadTourOperators();
   int max = 0;
   for (tour_operator & it : ops)
   {
      if (it.id > max)
      {
         max = it.id;
      }
   }
   return max + 1;
}



//
std::vector<vacation> CsvStorage::loadVacations()
{
   vacations_file_.close();
   vacations_file_.open(directory_name() + "vacations.csv", std::ios_base::in);
   std::string csvtext = read_csv_line_from(vacations_file_);
   StringTable table = Csv_parse(csvtext);
   std::vector<vacation> ops = createEntityVacationListFromTable(table);
   return ops;
}


void CsvStorage::saveVacations(const std::vector<vacation> &vacations)
{
   vacations_file_.close();
   vacations_file_.open(directory_name() + "vacations.csv", std::ios_base::out);
   std::vector<vacation> ops = vacations;
   StringTable table = createTableVacationFromEntityList(ops);
   std::string csvtext = Csv_toString(table);
   vacations_file_ << csvtext;
   vacations_file_.flush();
}

int CsvStorage::getNewVacationId()
{
   std::vector<vacation> ops = this->loadVacations();
   int max = 0;
   for (vacation &it : ops)
   {
      if (it.id > max)
      {
         max = it.id;
      }
   }
   return max + 1;
}

//

CsvStorage::~CsvStorage()
{
    tour_operators_file_.close();
    vacations_file_.close();
}
