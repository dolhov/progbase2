QT += xml
QT -= gui

CONFIG += c++17 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

QMAKE_CXXFLAGS+=-std=c++17
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    file_system.cpp \
    FileStorage.cpp \
    cui.cpp \
    csvstorage.cpp \
    xmlstorage.cpp

SUBDIRS += \
    project.pro

HEADERS += \
    vacation.h \
    tour_operators.h \
    file_system.h \
    cui.h \
    FileStorage.h \
    csvstorage.h \
    xmlstorage.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libcsvlab/release/ -llibcsvlab
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libcsvlab/debug/ -llibcsvlab
else:unix: LIBS += -L$$OUT_PWD/../libcsvlab/ -llibcsvlab

INCLUDEPATH += $$PWD/../libcsvlab
DEPENDPATH += $$PWD/../libcsvlab
