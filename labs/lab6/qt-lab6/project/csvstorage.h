#ifndef CSVSTORAGE_H
#define CSVSTORAGE_H

#include "FileStorage.h"

using namespace std;

class CsvStorage: public FileStorage
{
   // students
   vector<tour_operator> loadTourOperators();
   void saveTourOperators(const vector<tour_operator> & tour_ops);
   int getNewTourOperatorId();

   // courses
   vector<vacation> loadVacations();
   void saveVacations(const vector<vacation> & vacations);
   int getNewVacationId();

 public:
   explicit CsvStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
   ~CsvStorage();
};

#endif // CSVSTORAGE_H
