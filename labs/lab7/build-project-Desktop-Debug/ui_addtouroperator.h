/********************************************************************************
** Form generated from reading UI file 'addtouroperator.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDTOUROPERATOR_H
#define UI_ADDTOUROPERATOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddTourOperator
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QLabel *label_4;
    QLabel *label;
    QLineEdit *lineEdit_2;
    QLabel *label_3;
    QLineEdit *lineEdit_4;
    QLabel *label_5;
    QSpinBox *spinBox;

    void setupUi(QDialog *AddTourOperator)
    {
        if (AddTourOperator->objectName().isEmpty())
            AddTourOperator->setObjectName(QStringLiteral("AddTourOperator"));
        AddTourOperator->resize(299, 235);
        buttonBox = new QDialogButtonBox(AddTourOperator);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(70, 190, 151, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        gridLayoutWidget = new QWidget(AddTourOperator);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(30, 30, 231, 161));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        lineEdit_3 = new QLineEdit(gridLayoutWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        gridLayout->addWidget(lineEdit_3, 2, 2, 1, 1);

        lineEdit = new QLineEdit(gridLayoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        gridLayout->addWidget(lineEdit, 0, 2, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 4, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        lineEdit_2 = new QLineEdit(gridLayoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        gridLayout->addWidget(lineEdit_2, 1, 2, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        lineEdit_4 = new QLineEdit(gridLayoutWidget);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));

        gridLayout->addWidget(lineEdit_4, 4, 2, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        spinBox = new QSpinBox(gridLayoutWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximum(10000);

        gridLayout->addWidget(spinBox, 3, 2, 1, 1);


        retranslateUi(AddTourOperator);
        QObject::connect(buttonBox, SIGNAL(accepted()), AddTourOperator, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AddTourOperator, SLOT(reject()));

        QMetaObject::connectSlotsByName(AddTourOperator);
    } // setupUi

    void retranslateUi(QDialog *AddTourOperator)
    {
        AddTourOperator->setWindowTitle(QApplication::translate("AddTourOperator", "Dialog", Q_NULLPTR));
        label_2->setText(QApplication::translate("AddTourOperator", "City:", Q_NULLPTR));
        label_4->setText(QApplication::translate("AddTourOperator", "Currency:", Q_NULLPTR));
        label->setText(QApplication::translate("AddTourOperator", "Name: ", Q_NULLPTR));
        label_3->setText(QApplication::translate("AddTourOperator", "Vacation:", Q_NULLPTR));
        label_5->setText(QApplication::translate("AddTourOperator", "Cost:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AddTourOperator: public Ui_AddTourOperator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDTOUROPERATOR_H
