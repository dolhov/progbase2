#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QAction>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->actionOpen_Storage_2->setShortcuts(QKeySequence::New);
    ui->actionOpen_Storage_2->setStatusTip(tr("Open a file"));
    connect(ui->actionOpen_Storage_2, &QAction::triggered, this, &MainWindow::actionOpen_Storage_2);
    ui->actionNew_Storage_2->setShortcut(QKeySequence::New);
    ui->actionNew_Storage_2->setStatusTip(tr("Create a new file"));
    connect(ui->actionNew_Storage_2, &QAction::triggered, this, &MainWindow::actionNew_Storage_2);


}

MainWindow::~MainWindow()
{
    if (storage_ != nullptr)
    {
        delete storage_;
    }
    delete ui;
}


void MainWindow::actionOpen_Storage_2()
{
    ui->listWidget->clear();
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Dialog Caption",  // caption
                   "",                // directory to start with
                   "XML (*.xml);;All Files (*)");  // file name filter
       qDebug() << fileName;
       if (storage_ != nullptr)
       {
           delete storage_;
       }
       XmlStorage * stor;
       stor = new XmlStorage;
       storage_ = stor;
       storage_->setName(fileName.toStdString());
       storage_->open();
       std::vector<tour_operator> tourops = storage_->getAllTourOperators();
       for (tour_operator & tours : tourops)
       {
           QString text = QString::fromStdString(tours.name);
           QListWidgetItem * new_item = new QListWidgetItem(text);
           QVariant var = QVariant::fromValue(tours.id);
           new_item->setData(Qt::UserRole, var);
           ui->listWidget->addItem(new_item);
       }
}
void MainWindow::actionNew_Storage_2()
{
    QFileDialog dialog(this);
       dialog.setFileMode(QFileDialog::Directory);
       QString current_dir = QDir::currentPath();
       QString default_name = "new_storage";
       QString folder_path = dialog.getSaveFileName(
           this,
           "Select New Storage Folder",
           current_dir + "/" + default_name,
           "Folders");
       qDebug() << folder_path;
       XmlStorage * stor;
       stor = new XmlStorage;
       storage_ = stor;
       stor->setName(folder_path.toStdString() + ".xml");
       stor->open();
       this->actionOpen_Storage_2();
}

void MainWindow::on_pushButton_clicked()
{
    if (storage_ != nullptr)
    {
        AddTourOperator dialog(this);
        int status = dialog.exec();
        qDebug() << "status: " << status;
        if (status == 1)
        {
            tour_operator tourop = dialog.data();
            tourop.id = storage_->insertTourOperator(tourop);
            qDebug() << QString::fromStdString(tourop.name) << " | " << QString::fromStdString(tourop.city);

                QString text = QString::fromStdString(tourop.name);
                QListWidgetItem * new_item = new QListWidgetItem(text);
                QVariant var = QVariant::fromValue(tourop.id);
                new_item->setData(Qt::UserRole, var);
                ui->listWidget->addItem(new_item);
        }
    }
    else
     {
            QMessageBox::information(
                this,
                "Information",
                "Open a storage first");

     }
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    QString data = var.toString();
    tour_operator tourop = storage_->getTourOperatorById(var.toInt()).value();
    ui->label_3->setText(QString::fromStdString(tourop.name));
    ui->label_4->setText(QString::fromStdString(tourop.city));
    ui->label_8->setText(QString::fromStdString(tourop.vacation));
    ui->label_9->setText(QString::fromStdString(std::to_string(tourop.cost)));
    ui->label_10->setText(QString::fromStdString(tourop.currency));
}


void MainWindow::on_pushButton_2_clicked()
{
    QList list = ui->listWidget->selectedItems();
    if (storage_ != nullptr)
    {
        if (list.size() == 0)
        {
            QMessageBox::information(
                this,
                "Information",
                "Choose entity");
        }
        else
        {
            Edit dialog(this);
            int status = dialog.exec();
            qDebug() << "status: " << status;

            if (status == 1)
            {

    //            QVariant var = item->data(Qt::UserRole);
    //            QString data = var.toString();

                QListWidgetItem * item = list.at(list.size() - 1);
                QVariant var = item->data(Qt::UserRole);
                tour_operator tourop = dialog.data();
                tour_operator tourops = storage_->getTourOperatorById(var.toInt()).value();
                if (tourops.name != tourop.name && tourop.name != "")
                {
                    tourops.name = tourop.name;
                }
                if (tourops.city != tourop.city && tourop.city != "")
                {
                    tourops.city = tourop.city;
                }
                if (tourops.vacation != tourop.vacation && tourop.vacation != "")
                {
                    tourops.vacation = tourop.vacation;
                }
                if (tourops.currency != tourop.currency && tourop.currency != "")
                {
                    tourops.currency = tourop.currency;
                }
                if (tourops.cost != tourop.cost && tourop.cost != 0)
                {
                    tourops.cost = tourop.cost;
                }
                storage_->updateTourOperator(tourops);
                ui->listWidget->clear();
                std::vector<tour_operator> touroperators = storage_->getAllTourOperators();
                for (tour_operator & tours : touroperators)
                {
                    QString text = QString::fromStdString(tours.name);
                    QListWidgetItem * new_item = new QListWidgetItem(text);
                    QVariant var = QVariant::fromValue(tours.id);
                    new_item->setData(Qt::UserRole, var);
                    ui->listWidget->addItem(new_item);
                }
        }

        }
    }
    else
    {
        QMessageBox::information(
            this,
            "Information",
            "Open a storage first");

    }
}

void MainWindow::on_pushButton_3_clicked()
{
    if (storage_ != nullptr)
    {
        QList list = ui->listWidget->selectedItems();
        if (list.size() == 0)
        {
            QMessageBox::information(
                this,
                "Information",
                "Choose entity");
        }
        else
        {
            QMessageBox::StandardButton reply;
               reply = QMessageBox::question(
                   this,
                   "On delete",
                   "Are you sure?",
                   QMessageBox::Yes|QMessageBox::No);
               if (reply == QMessageBox::Yes) {
                   qDebug() << "Yes was clicked";
                   QListWidgetItem * item = list.at(list.size() - 1);
                   QVariant var = item->data(Qt::UserRole);
                   storage_->removeTourOperator(var.toInt());
                   QListWidgetItem * it = ui->listWidget->takeItem(ui->listWidget->currentRow());
                   delete it;
               } else {
                   qDebug() << "Yes was *not* clicked";
               }

    //            QVariant var = item->data(Qt::UserRole);
    //            QString data = var.toString();
        }

    }
    else
    {
        QMessageBox::information(
            this,
            "Information",
            "Open a storage first");
    }
}
