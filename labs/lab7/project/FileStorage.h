#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <optional>

#include "tour_operators.h"
#include "vacation.h"
#include "file_system.h"



class FileStorage
{
   std::string  dir_name_;
protected :
   std::fstream tour_operators_file_;
   std::fstream vacations_file_;

   // tour operators
   virtual std::vector<tour_operator> loadTourOperators() = 0;
   virtual void saveTourOperators(const std::vector<tour_operator> & tour_operators) = 0;
   virtual int getNewTourOperatorId() = 0;

   // vacations
   virtual std::vector<vacation> loadVacations() = 0;
   virtual void saveVacations(const std::vector<vacation> & vacations) = 0;
   virtual int getNewVacationId() = 0;

 public:
   explicit FileStorage(const std::string & dir_name = "");
   virtual ~FileStorage() {}

   void setName(const std::string & dir_name);
   std::string name() const;

   bool isOpen() const;
   bool open(); 
   void close();

   // tour operators
   std::vector<tour_operator> getAllTourOperators(void);
   std::optional<tour_operator> getTourOperatorById(int operator_id);
   bool updateTourOperator(const tour_operator &tour_op);
   bool removeTourOperator(int operator_id);
   int insertTourOperator(const tour_operator &tour_operator);

   // vacations
   std::vector<vacation> getAllVacations(void);
   std::optional<vacation> getVacationById(int vacation_id);
   bool updateVacation(const vacation & vacation_upd);
   bool removeVacation(int vacation_id);
   int insertVacation(const vacation &vacation_ins);
   std::string directory_name();
};
