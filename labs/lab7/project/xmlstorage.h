#ifndef XMLSTORAGE_H
#define XMLSTORAGE_H

#include "FileStorage.h"
#include <QtXml>

using namespace std;

class XmlStorage: public FileStorage
{
   // tour operators
   vector<tour_operator> loadTourOperators();
   void saveTourOperators(const vector<tour_operator> & tour_operators);
   int getNewTourOperatorId();

   // courses
   vector<vacation> loadVacations();
   void saveVacations(const vector<vacation> & vacations);
   int getNewVacationId();

 public:
   explicit XmlStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
};

#endif // XMLSTORAGE_H
