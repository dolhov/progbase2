#ifndef EDIT_H
#define EDIT_H

#include <QDialog>
#include "tour_operators.h"

namespace Ui {
class Edit;
}

class Edit : public QDialog
{
    Q_OBJECT

public:
    explicit Edit(QWidget *parent = 0);
    ~Edit();
    tour_operator data();

private:
    Ui::Edit *ui;
};

#endif // EDIT_H
