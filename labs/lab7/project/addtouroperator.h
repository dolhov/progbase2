#ifndef ADDTOUROPERATOR_H
#define ADDTOUROPERATOR_H

#include <QDialog>
#include <tour_operators.h>

namespace Ui {
class AddTourOperator;
}

class AddTourOperator : public QDialog
{
    Q_OBJECT

public:
    explicit AddTourOperator(QWidget *parent = 0);
    ~AddTourOperator();
    tour_operator data();
private:
    Ui::AddTourOperator *ui;
};

#endif // ADDTOUROPERATOR_H
