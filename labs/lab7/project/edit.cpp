#include "edit.h"
#include "ui_edit.h"

Edit::Edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Edit)
{
    ui->setupUi(this);
}

Edit::~Edit()
{
    delete ui;
}


tour_operator Edit::data()
{
    tour_operator tourop;
    tourop.name = ui->lineEdit->text().toStdString();
    tourop.city = ui->lineEdit_2->text().toStdString();
    tourop.vacation = ui->lineEdit_3->text().toStdString();
    tourop.cost = ui->spinBox->value();
    tourop.currency = ui->lineEdit_4->text().toStdString();
    return tourop;
}
