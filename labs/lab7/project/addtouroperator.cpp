#include "addtouroperator.h"
#include "ui_addtouroperator.h"

AddTourOperator::AddTourOperator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddTourOperator)
{
    ui->setupUi(this);
}

AddTourOperator::~AddTourOperator()
{
    delete ui;
}


tour_operator AddTourOperator::data()
{
    tour_operator tourop;
    tourop.name = ui->lineEdit->text().toStdString();
    tourop.city = ui->lineEdit_2->text().toStdString();
    tourop.vacation = ui->lineEdit_3->text().toStdString();
    tourop.cost = ui->spinBox->value();
    tourop.currency = ui->lineEdit_4->text().toStdString();
    return tourop;
}
