#include "FileStorage.h"

FileStorage::FileStorage(const std::string & dir_name)
{
   dir_name_ = dir_name;
}

void FileStorage::setName(const std::string &dir_name)
{
   dir_name_ = dir_name;
}

std::string FileStorage::name() const
{
   return dir_name_;
}

bool FileStorage::isOpen() const
{
   return tour_operators_file_.is_open();
}

bool FileStorage::open()
{
   tour_operators_file_.open(dir_name_ + "tour_operators.csv");
   return tour_operators_file_.is_open();
}

void FileStorage::close()
{
   tour_operators_file_.close();
}

// tour operators
std::vector<tour_operator> FileStorage::getAllTourOperators(void)
{
   return this->loadTourOperators();
}

std::optional<tour_operator> FileStorage::getTourOperatorById(int operator_id)
{
   std::vector<tour_operator> tours = this->loadTourOperators();
   for (tour_operator & st : tours)
   {
      if (st.id == operator_id)
      {
         return std::optional<tour_operator>{st};
      }
   }
   return std::optional<tour_operator>{};
}

bool FileStorage::updateTourOperator(const tour_operator &tour_op)
{
   std::vector<tour_operator> ops = this->loadTourOperators();
   for (tour_operator & st : ops)
   {
      if (st.id == tour_op.id)
      {
         st = tour_op;
         this->saveTourOperators(ops);
         return true;
      }
   }
   return false;
}


bool FileStorage::removeTourOperator(int operator_id)
{
   std::vector<tour_operator> ops = this->loadTourOperators();
   auto it = ops.begin();
   auto it_end = ops.end();
   for (; it != it_end; it++)
   {
      if (it->id == operator_id)
      {
         ops.erase(it);
         this->saveTourOperators(ops);
         return true;
      }
   }
   return false;
}

int FileStorage::insertTourOperator(const tour_operator & tour_operator_another)
{
   int new_id = this->getNewTourOperatorId();
   tour_operator copy = tour_operator_another;
   copy.id = new_id;
   std::vector<tour_operator> ops = this->loadTourOperators();
   ops.push_back(copy);
   this->saveTourOperators(ops);
   return new_id;

}


//
//
//


std::vector<tour_operator> FileStorage::loadTourOperators()
{
   tour_operators_file_.close();
   tour_operators_file_.open(dir_name_ + "tour_operators.csv", std::ios_base::in);
   std::string csvtext = read_csv_line_from(tour_operators_file_);
   StringTable table = Csv_parse(csvtext);
   std::vector<tour_operator> ops = createEntityListFromTable(table);
   return ops;
}

void FileStorage::saveTourOperators(const std::vector<tour_operator> & tour_operators)
{
   tour_operators_file_.close();
   tour_operators_file_.open(dir_name_ + "tour_operators.csv", std::ios_base::out);
   std::vector<tour_operator> ops = tour_operators;
   StringTable table = createTableFromEntityList(ops);
   std::string csvtext = Csv_toString(table);
   tour_operators_file_ << csvtext;
   tour_operators_file_.flush();
}

int FileStorage::getNewTourOperatorId()
{
   std::vector<tour_operator> ops = this->loadTourOperators();
   int max = 0;
   for (tour_operator & it : ops)
   {
      if (it.id > max)
      {
         max = it.id;
      }
   }
   return max + 1;
}



//
std::vector<vacation> FileStorage::loadVacations()
{
   vacations_file_.close();
   vacations_file_.open(dir_name_ + "vacations.csv", std::ios_base::in);
   std::string csvtext = read_csv_line_from(vacations_file_);
   StringTable table = Csv_parse(csvtext);
   std::vector<vacation> ops = createEntityVacationListFromTable(table);
   return ops;
}


void FileStorage::saveVacations(const std::vector<vacation> &vacations)
{
   vacations_file_.close();
   vacations_file_.open(dir_name_ + "vacations.csv", std::ios_base::out);
   std::vector<vacation> ops = vacations;
   StringTable table = createTableVacationFromEntityList(ops);
   std::string csvtext = Csv_toString(table);
   vacations_file_ << csvtext;
   vacations_file_.flush();
}

int FileStorage::getNewVacationId()
{
   std::vector<vacation> ops = this->loadVacations();
   int max = 0;
   for (vacation &it : ops)
   {
      if (it.id > max)
      {
         max = it.id;
      }
   }
   return max + 1;
}

//


std::vector<vacation> FileStorage::getAllVacations(void)
{
   return this->loadVacations();
}

std::optional<vacation> FileStorage::getVacationById(int vacation_id)
{
   std::vector<vacation> vacations = this->loadVacations();
   for (vacation & st : vacations)
   {
      if (st.id == vacation_id)
      {
         return std::optional<vacation>{st};
      }
   }
   return std::optional<vacation>{};
}


bool FileStorage::updateVacation(const vacation &vacation_upd)
{
   std::vector<vacation> ops = this->loadVacations();
   for (vacation & st : ops)
   {
      if (st.id == vacation_upd.id)
      {
         st = vacation_upd;
         this->saveVacations(ops);
         return true;
      }
   }
   return false;
}


bool FileStorage::removeVacation(int vacation_id)
{
   std::vector<vacation> ops = this->loadVacations();
   auto it = ops.begin();
   auto it_end = ops.end();
   for (; it != it_end; it++)
   {
      if (it->id == vacation_id)
      {
         ops.erase(it);
         this->saveVacations(ops);
         return true;
      }
   }
   return false;
}


int FileStorage::insertVacation(const vacation &vacation_ins)
{
   int new_id = this->getNewVacationId();
   vacation copy = vacation_ins;
   copy.id = new_id;
   std::vector<vacation> ops = this->loadVacations();
   ops.push_back(copy);
   this->saveVacations(ops);
   return new_id;
}
