#pragma once
#include <iostream>
#include "FileStorage.h"
#include "tour_operators.h"

class Cui
{
    FileStorage * const storage_;

    // students menus
    void operatorsMainMenu();
    void operatorsMenu(int entity_id);
    void operatorsUpdateMenu(int entity_id);
    void operatorsDeleteMenu(int entity_id);
    void operatorsCreateMenu();

    // vacation menus
    void vacationsMainMenu();
    void vacationMenu(int id);
    void vacationUpdateMenu(int id);
    void vacationDeleteMenu(int entity_id);
    void vacationCreateMenu();
    
public:
    Cui(FileStorage * storage): storage_{storage} {}
    //
    void show();
};