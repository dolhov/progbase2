#pragma once
#include <string>

struct vacation
{
    int id;
    std::string city;
    int duration;
    std::string hotel_name;
};
