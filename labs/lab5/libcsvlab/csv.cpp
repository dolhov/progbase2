#include "csv.h"

StringTable Csv_parse(std::string &csvStr)
{
    size_t cols = 0;
    for (int i = 0; csvStr[i] != '\n'; i++)
    {
        if (csvStr[i] == ',')
        {
            cols += 1;
        }
    }
    cols += 1;
    size_t size = csvStr.size();
    size_t rows = 0;
    for (int i = 0; i < size; i++)
    {
        if (csvStr[i] == '\n')
        {
            rows += 1;
        }
    }
    StringTable table(rows, cols);
    {
        int q = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                std::string& string = table.at(i, j);
                for (; csvStr[q] != '\n'; q++)
                {
                    if (csvStr[q] == ',')
                    {
                        q++;
                        break;
                    }
                    string += csvStr[q];
                }
                if (csvStr[q] == '\n')
                {
                    q++;
                    break;
                }
            }
        }
    }
    return table;
}

// create string and fill it with csv text 
std::string Csv_toString(StringTable &table)
{
    std::string string;
    size_t col = table.size_columns();
    size_t row = table.size_rows();
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            string += table.at(i, j);
            if (j != col - 1)
            {
                string += ',';
            }
        }
        if (i != row - 1)
        {
            string += '\n';
        }
    }
    if (string[string.size() - 1] == '\r')
    {
        string.pop_back();
    }
    return string;
}