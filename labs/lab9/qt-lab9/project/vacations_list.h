#ifndef VACATIONS_LIST_H
#define VACATIONS_LIST_H

#include <QDialog>
#include "vacation.h"
#include <optional>

namespace Ui {
class vacations_list;
}

class vacations_list : public QDialog
{
    Q_OBJECT
    std::vector<vacation> vacs;

public:
    explicit vacations_list(QWidget *parent = 0);
    ~vacations_list();

    void setVacations(std::vector<vacation> vacs);
    std::optional<int> vacationId();
    void DisplayVacations();


private:
    Ui::vacations_list *ui;
};

#endif // VACATIONS_LIST_H
