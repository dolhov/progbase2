#include "auntefication.h"
#include "ui_auntefication.h"

auntefication::auntefication(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::auntefication)
{
    ui->setupUi(this);
}

auntefication::~auntefication()
{
    delete ui;
}

std::vector<std::string> auntefication::login()
{
    std::string userName;
    std::string password;
    std::vector<std::string> vec;
    userName = ui->lineEdit->text().toStdString();
    password = ui->lineEdit_2->text().toStdString();
    vec.push_back(userName);
    vec.push_back(password);
    return vec;
}

void auntefication::on_buttonBox_accepted()
{
   this->close();
}
