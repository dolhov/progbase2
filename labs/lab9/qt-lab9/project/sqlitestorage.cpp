#include "sqlitestorage.h"


//password
QString SqliteStorage::hashPassword(QString const & pass) {
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}
//

SqliteStorage::SqliteStorage(const string & dir_name)
{
    this->set_directory_name(dir_name);
    database_ = QSqlDatabase::addDatabase("QSQLITE");
}


vector<tour_operator> SqliteStorage::getAllTourOperators(void)
{
    vector<tour_operator> stor;
    this->open();
    QSqlQuery query("SELECT * FROM tour_operators");
    while (query.next())
    {
       string name = query.value("name").toString().toStdString();
       int id = query.value("id").toInt();
       string city = query.value("city").toString().toStdString();
       string vacation = query.value("vacation").toString().toStdString();
       int cost = query.value("cost").toInt();
       string currency = query.value("currency").toString().toStdString();
       tour_operator tour_op;
       tour_op.id = id;
       tour_op.city = city;
       tour_op.vacation = vacation;
       tour_op.cost = cost;
       tour_op.currency = currency;
       tour_op.name = name;
       stor.push_back(tour_op);

       //qDebug() << id << " | " << QString::fromStdString(name);
    }
    return stor;
}

optional<tour_operator> SqliteStorage::getTourOperatorById(int operator_id)
{
    QSqlQuery query;
    if (!database_.isOpen())
    {
        this->open();
    }
    if (!query.prepare("SELECT * FROM tour_operators WHERE id = :id"))
    {
       qDebug() << "get person query prepare error:";// << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":id", operator_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get person query exec error:";// << query.lastError();
       // return or throw or do smth else
    }
    if (query.next()) {
        string name = query.value("name").toString().toStdString();
        int id = query.value("id").toInt();
        string city = query.value("city").toString().toStdString();
        string vacation = query.value("vacation").toString().toStdString();
        int cost = query.value("cost").toInt();
        string currency = query.value("currency").toString().toStdString();
        tour_operator tour_op;
        tour_op.id = id;
        tour_op.city = city;
        tour_op.vacation = vacation;
        tour_op.cost = cost;
        tour_op.currency = currency;
        tour_op.name = name;
       return tour_op;
    } else {
       qDebug() << " not found ";
       return nullopt;
    }
}

bool SqliteStorage::updateTourOperator(const tour_operator &tour_op)
{
    QSqlQuery query;
    if (!query.prepare("UPDATE tour_operators SET name = :name, city = :city, vacation = :vacation, cost = :cost, currency = :currency WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:";// << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":name", QString::fromStdString(tour_op.name));
    query.bindValue(":city",  QString::fromStdString(tour_op.city));
    query.bindValue(":vacation",  QString::fromStdString(tour_op.vacation));
    query.bindValue(":cost", tour_op.cost);
    query.bindValue(":currency",  QString::fromStdString(tour_op.currency));
    query.bindValue(":id",  tour_op.id);
    if (!query.exec()){
        qDebug() << "updatePerson query exec error:";// << query.lastError();
        // return or throw or do smth else
    }
}

bool SqliteStorage::removeTourOperator(int operator_id)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("DELETE FROM tour_operators WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:"; // << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":id", operator_id);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:"; //<< query.lastError();
        // return or throw or do smth else
    }
}

int SqliteStorage::insertTourOperator(const tour_operator &tour_op)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("INSERT INTO tour_operators (name, city, vacation, cost, currency, user_id) VALUES (:name, :city, :vacation, :cost, :currency, :user_id)"))
    {
       qDebug() << "addTourOperator query prepare error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":name", QString::fromStdString(tour_op.name));
    query.bindValue(":city",  QString::fromStdString(tour_op.city));
    query.bindValue(":vacation",  QString::fromStdString(tour_op.vacation));
    query.bindValue(":cost", tour_op.cost);
    query.bindValue(":currency",  QString::fromStdString(tour_op.currency));
    query.bindValue(":user_id", tour_op.user_id);
    if (!query.exec())
    {
       qDebug() << "addPerson query exec error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    return query.lastInsertId().toInt();
}

// courses
vector<vacation> SqliteStorage::getAllVacations(void)
{
    vector<vacation> stor;
    this->open();
    QSqlQuery query("SELECT * FROM vacations");
    while (query.next())
    {

       int id = query.value("id").toInt();
       string city = query.value("city").toString().toStdString();
       int duration = query.value("duration").toInt();
       string hotel_name = query.value("hotel_name").toString().toStdString();
       vacation vac;
       vac.id = id;
       vac.city = city;
       vac.duration = duration;
       vac.hotel_name = hotel_name;
       //qDebug() << id << " | " << QString::fromStdString(city);
       stor.push_back(vac);
    }
    return stor;
}

optional<vacation> SqliteStorage::getVacationById(int vacation_id)
{
    QSqlQuery query;
    if (!database_.isOpen())
    {
        this->open();
    }
    if (!query.prepare("SELECT * FROM vacations WHERE id = :id"))
    {
       qDebug() << "get person query prepare error:";// << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":id", vacation_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get person query exec error:";// << query.lastError();
       // return or throw or do smth else
    }
    if (query.next()) {
       qDebug() << " found ";
       int id = query.value("id").toInt();
       string city = query.value("city").toString().toStdString();
       int duration = query.value("duration").toInt();
       string hotel_name = query.value("hotel_name").toString().toStdString();
       vacation vac;
       vac.id = id;
       vac.city = city;
       vac.duration = duration;
       vac.hotel_name = hotel_name;
       return vac;
    } else {
       qDebug() << " not found ";
       return nullopt;
    }

}

bool SqliteStorage::updateVacation(const vacation & vacation_upd)
{
    QSqlQuery query;
    if (!query.prepare("UPDATE vacations SET hotel_name = :hotel_name, city = :city, duration = :duration WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:";// << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":hotel_name", QString::fromStdString(vacation_upd.hotel_name));
    query.bindValue(":city",  QString::fromStdString(vacation_upd.city));
    query.bindValue(":duration", vacation_upd.duration);
    query.bindValue(":id", vacation_upd.id);
    if (!query.exec()){
        qDebug() << "updateVacation query exec error:";// << query.lastError();
        // return or throw or do smth else
        return false;
    }
    return true;
}

bool SqliteStorage::removeVacation(int vacation_id)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("DELETE FROM vacations WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:";// << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":id", vacation_id);
    if (!query.exec()){
        qDebug() << "removeVacation query exec error:"; //<< query.lastError();
        // return or throw or do smth else
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertVacation(const vacation &vac)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("INSERT INTO vacations (city, duration, hotel_name) VALUES (:city, :duration, :hotel_name)"))
    {
       qDebug() << "vacations query prepare error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":hotel_name", QString::fromStdString(vac.hotel_name));
    query.bindValue(":city",  QString::fromStdString(vac.city));
    query.bindValue(":duration", vac.duration);
    if (!query.exec())
    {
       qDebug() << "insertVacation query exec error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    return query.lastInsertId().toInt();
}



//Users
optional<User> SqliteStorage::getUserAuth(
    const string & username,
    const string & password)
{
    if(this->open())
    {
        QSqlQuery query;
        query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash;");
        query.bindValue(":username", QString::fromStdString(username));
        query.bindValue(":password_hash", hashPassword(QString::fromStdString(password)));
        User usr;
        if (!query.exec()) {  // do exec if query is prepared SELECT query
    qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
           // return or throw or do smth else
        }
        if (query.next())
        {
           usr.id = query.value("id").toInt();
           usr.username = query.value("username").toString().toStdString();
           usr.password_hash = query.value("password_hash").toString().toStdString();
           return usr;
        }
        return std::nullopt;
    }

}
vector<tour_operator> SqliteStorage::getAllUserTourOperators(int user_id)
{
    if(this->open())
    {
        QSqlQuery query;
        query.prepare("SELECT * FROM tour_operators WHERE user_id = :user_id");
        query.bindValue(":user_id", user_id);
        vector<tour_operator> stor;
        if (!query.exec()) {  // do exec if query is prepared SELECT query
           qDebug() << "get allUserOperators query exec error:";// << query.lastError();
           // return or throw or do smth else
        }
        while (query.next())
        {
            string name = query.value("name").toString().toStdString();
            int id = query.value("id").toInt();
            string city = query.value("city").toString().toStdString();
            string vacation = query.value("vacation").toString().toStdString();
            int cost = query.value("cost").toInt();
            string currency = query.value("currency").toString().toStdString();
            tour_operator tour_op;
            tour_op.id = id;
            tour_op.city = city;
            tour_op.vacation = vacation;
            tour_op.cost = cost;
            tour_op.currency = currency;
            tour_op.name = name;
            stor.push_back(tour_op);
        }
        return stor;
    }
}

// links
vector<vacation> SqliteStorage::getAllTourOperatorsVacations(int tourop_id)
{
    if(this->open())
    {
        QSqlQuery query;
        query.prepare("SELECT * FROM links WHERE tour_operator_id = :tourop_id");
        query.bindValue(":tourop_id", tourop_id);
        vector<vacation> stor;
        if (!query.exec()) {  // do exec if query is prepared SELECT query
           qDebug() << "get allTourOperators (links) query exec error:";// << query.lastError();
           // return or throw or do smth else
        }
        while (query.next())
        {
            int id = query.value("vacation_id").toInt();
            optional<vacation> op (this->getVacationById(id));
            if (op)
            {
                stor.push_back(op.value());
            }
        }
        return stor;
    }
}
bool SqliteStorage::insertTourOperatorVacation(int tourop_id, int vacation_id)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    vector<vacation> vc = this->getAllTourOperatorsVacations(tourop_id);
    for (vacation vac : vc)
    {
        if (vacation_id == vac.id)
        {
            return false;
        }
    }
    if (!query.prepare("INSERT INTO links (tour_operator_id, vacation_id) VALUES (:tourop_id, :vacation_id)"))
    {
       qDebug() << "addTourOperator query prepare error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":tourop_id", tourop_id);
    query.bindValue(":vacation_id", vacation_id);
    if (!query.exec())
    {
       qDebug() << "addLink query exec error:";
//                << query.lastError();
       // return or throw or do smth else
       return false;
    }
    return true;
}

bool SqliteStorage::removeTourOperatorVacation(int tourop_id, int vacation_id)
{

    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("DELETE FROM links WHERE tour_operator_id = :tour_operator_id AND vacation_id = :vacation_id")){
        qDebug() << "deletePerson query prepare error: " << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":tour_operator_id", tourop_id);
    query.bindValue(":vacation_id", vacation_id);
    if (!query.exec()){
        qDebug() << "deleteLink query exec error: " << query.lastError();
        return false;
        // return or throw or do smth else
    }
    return true;
}

bool SqliteStorage::isOpen() const
{
    return database_.isOpen();
}

bool SqliteStorage::open()
{
    if (!database_.isOpen())
    {
        QString path = QString::fromStdString(directory_name());
        if (!QFileInfo::exists(path))
        {
           qDebug() << "Database does not exist:" << path;
           return false;
           // return throw or do smth else
        }


        database_.setDatabaseName(path);    // set sqlite database file path
        bool connected = database_.open();  // open db connection
        if (!connected) {
           qDebug() << "open database error:";
           return false;
        //<< database_.lastError();
           // return or throw or do smth else
        }
        //
        // do smth with database here
        //
        return true;
    }
    return true;
}


void SqliteStorage::close()
{
    if (database_.isOpen())
    {
        QString st;
        {
            database_.close();
            QSqlDatabase db = database_.database();
            st = db.connectionName();
            database_.~QSqlDatabase();
        }
        QSqlDatabase::removeDatabase(st);

    }
}
