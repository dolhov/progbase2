#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "FileStorage.h"
#include "sqlitestorage.h"
#include "addtouroperator.h"
#include "edit.h"
#include "auntefication.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_pushButton_clicked();
    void actionOpen_Storage_2();
    void actionNew_Storage_2();
    void actionLogout();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();
    bool logIn();

private:
    Ui::MainWindow *ui;
    Storage * storage_;
    int user_loged_id;
};

#endif // MAINWINDOW_H
