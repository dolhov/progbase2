#pragma once

#include <string>
#include <vector>
#include <optional>
#include <QCryptographicHash>

#include "tour_operators.h"
#include "vacation.h"
#include "user.h"

using namespace std;

class Storage
{
 private:
   string dir_name_;

 public:
   Storage();
   explicit Storage(const string & dir_name);
   virtual ~Storage() {}

   void setName(const string & dir_name);
   string name() const;

   virtual bool isOpen() const = 0;
   virtual bool open() = 0;
   virtual void close() = 0;

   // students
   virtual vector<tour_operator> getAllTourOperators(void) = 0;
   virtual optional<tour_operator> getTourOperatorById(int operator_id) = 0;
   virtual bool updateTourOperator(const tour_operator & tour_op) = 0;
   virtual bool removeTourOperator(int operator_id) = 0;
   virtual int insertTourOperator(const tour_operator &tour_op) = 0;

   // courses
   virtual vector<vacation> getAllVacations(void) = 0;
   virtual optional<vacation> getVacationById(int vacation_id) = 0;
   virtual bool updateVacation(const vacation & vacation_upd) = 0;
   virtual bool removeVacation(int vacation_id) = 0;
   virtual int insertVacation(const vacation &vac) = 0;

   // users
      virtual optional<User> getUserAuth(
          const string & username,
          const string & password) = 0;
      virtual vector<tour_operator> getAllUserTourOperators(int usr_id) = 0;

      // links
      virtual vector<vacation> getAllTourOperatorsVacations(int tourop_id) = 0;
      virtual bool insertTourOperatorVacation(int tourop_id, int vacation_id) = 0;
      virtual bool removeTourOperatorVacation(int tourop_id, int vacation_id) = 0;

   //
   string directory_name();
   void set_directory_name(string str);
};
