#include "vacations_list.h"
#include "ui_vacations_list.h"

vacations_list::vacations_list(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::vacations_list)
{
    ui->setupUi(this);
}

vacations_list::~vacations_list()
{
    delete ui;
}


std::optional<int> vacations_list::vacationId()
{
    QList list = ui->listWidget->selectedItems();
    if (list.size() != 0)
    {
        QListWidgetItem * item = list.at(list.size() - 1);
        QVariant var = item->data(Qt::UserRole);
        return var.toInt();
    }
    return std::nullopt;
}

void vacations_list::setVacations(std::vector<vacation> vacs)
{
    this->vacs = vacs;
    this->DisplayVacations();
}


void vacations_list::DisplayVacations()
{
    ui->listWidget->clear();
    for (vacation vc : this->vacs)
    {
        QString text = "id";
        text += QString::fromStdString(std::to_string(vc.id));
        text +=" ";
        text += QString::fromStdString(vc.city);
        text += ";";

        QListWidgetItem * new_item = new QListWidgetItem(text);
        QVariant var = QVariant::fromValue(vc.id);
        new_item->setData(Qt::UserRole, var);
        ui->listWidget->addItem(new_item);
    }
}
