#include "xmlstorage.h"

std::vector<tour_operator> traverse(const QDomNode &node, std::vector<tour_operator> list)
{
    tour_operator op;
    QDomNode domnode = node.firstChild();
    while(!domnode.isNull())
    {
        if (domnode.isElement())
        {
            QDomElement domElement = domnode.toElement();
            if (!domElement.isNull())
            {
                if (domElement.tagName() == "tour_operator")
                {
                    op.id = domElement.attribute("id","").toInt();
                    op.name = domElement.attribute("name","").toStdString();
                    op.city = domElement.attribute("city","").toStdString();
                    op.vacation = domElement.attribute("vacation","").toStdString();
                    op.cost = domElement.attribute("cost","").toInt();
                    op.currency = domElement.attribute("currency","").toStdString();
                    list.push_back(op);
                }
            }
        }
        traverse(domnode, list);
        domnode = domnode.nextSibling();
    }
    return list;
}

std::vector<vacation> traverse_vacations(const QDomNode &node, std::vector<vacation> list)
{
    vacation op;
    QDomNode domnode = node.firstChild();
    while(!domnode.isNull())
    {
        if (domnode.isElement())
        {
            QDomElement domElement = domnode.toElement();
            if (!domElement.isNull())
            {
                if (domElement.tagName() == "tour_operator")
                {
                    op.id = domElement.attribute("id","").toInt();
                    op.duration = std::stoi(domElement.attribute("duration","").toStdString());
                    op.city = domElement.attribute("city","").toStdString();
                    op.hotel_name = domElement.attribute("hotel_name","").toStdString();
                    list.push_back(op);
                }
            }
        }
        traverse_vacations(domnode, list);
        domnode = domnode.nextSibling();
    }
    return list;
}

vector<tour_operator> XmlStorage::loadTourOperators()
{
    std::string checker = directory_name() + "tour_operators.xml";
    QString st = QString::fromStdString(checker);
    QDomDocument xmlDocument;
       QFile f(st);
       f.open(QIODevice::ReadOnly);
       if(!f.isOpen())
       {
           qDebug("Error While Reading the File");
       }
       xmlDocument.setContent(&f);
       f.close();
    QDomElement el = xmlDocument.documentElement();
    vector<tour_operator> tour_operators_list;
    tour_operators_list = traverse(el, tour_operators_list);
    return tour_operators_list;
}

void XmlStorage::saveTourOperators(const vector<tour_operator> & tour_ops)
{
    std::string checker = directory_name() + "tour_operators.xml";
    QString st = QString::fromStdString(checker);
        QDomDocument doc;
        QDomElement root = doc.createElement("tour_operators");
        doc.appendChild(root);
        for (int i = 0; i < tour_ops.size(); i++)
        {
            QDomElement ent = doc.createElement("tour_operator");
            ent.setTagName("tour_operator");
            ent.setAttribute("id", tour_ops[i].id);
            ent.setAttribute("name", QString::fromStdString(tour_ops[i].name));
            ent.setAttribute("city", QString::fromStdString(tour_ops[i].city));
            ent.setAttribute("vacation", QString::fromStdString(tour_ops[i].vacation));
            ent.setAttribute("cost", tour_ops[i].cost);
            ent.setAttribute("currency", QString::fromStdString(tour_ops[i].currency));
            root.appendChild(ent);
        }
        QFile file(st);
            if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                qDebug() << "Open the file for writing failed";
            }
            else
            {
                QTextStream stream(&file);
                QString l = doc.toString();
                stream << doc.toString();
                file.close();
                qDebug() << "Writing is done";
            }
}

int XmlStorage::getNewTourOperatorId()
{
    std::vector<tour_operator> ops = this->loadTourOperators();
    int max = 0;
    for (tour_operator & it : ops)
    {
       if (it.id > max)
       {
          max = it.id;
       }
    }
    return max + 1;
}

vector<vacation> XmlStorage::loadVacations()
{
    std::string checker = directory_name() + "vacations.xml";
    QString st = QString::fromStdString(checker);
    QDomDocument xmlDocument;
       QFile f(st);
       f.open(QIODevice::ReadOnly);
       if(!f.isOpen())
       {
           qDebug("Error While Reading the File");
       }
       xmlDocument.setContent(&f);
       f.close();
    QDomElement el = xmlDocument.documentElement();
    vector<vacation> vacations;
    vacations = traverse_vacations(el, vacations);
    return vacations;
}
void  XmlStorage::saveVacations(const vector<vacation> & vacations)
{
    std::string checker = directory_name() + "vacations.xml";
    QString st = QString::fromStdString(checker);
        QDomDocument doc;
        QDomElement root = doc.createElement("vacations");
        doc.appendChild(root);
        for (int i = 0; i < vacations.size(); i++)
        {
            QDomElement ent = doc.createElement("vacation");
            ent.setTagName("tour_operator");
            ent.setAttribute("id", vacations[i].id);
            ent.setAttribute("city", QString::fromStdString(vacations[i].city));
            ent.setAttribute("duration", vacations[i].duration);
            ent.setAttribute("hotel_name", QString::fromStdString(vacations[i].hotel_name));
            root.appendChild(ent);
        }
        QFile file(st);
            if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                qDebug() << "Open the file for writing failed";
            }
            else
            {
                QTextStream stream(&file);
                QString l = doc.toString();
                stream << doc.toString();
                file.close();
                qDebug() << "Writing is done";
            }
}

int  XmlStorage::getNewVacationId()
{
    std::vector<vacation> vacs = this->loadVacations();
    int max = 0;
    for (vacation & it : vacs)
    {
       if (it.id > max)
       {
          max = it.id;
       }
    }
    return max + 1;
}


optional<User> XmlStorage::getUserAuth(
    const string & username,
    const string & password)
{
    int i = 0;
}
vector<tour_operator> XmlStorage::getAllUserOperators(int tourop_id)
{
    int i = 0;
}

// links
 vector<vacation> XmlStorage::getAllTourOperatorsVacations(int tourop_id)
 {
     int i = 0;
 }
 bool XmlStorage::insertTourOperatorVacation(int tourop_id, int vacation_id)
 {
     int i = 0;
 }
 bool XmlStorage::removeTourOperatorVacation(int tourop_id, int vacation_id)
 {
     int i = 0;
 }

