#ifndef CSVSTORAGE_H
#define CSVSTORAGE_H

#include "FileStorage.h"

using namespace std;

class CsvStorage: public FileStorage
{
   // students
   vector<tour_operator> loadTourOperators();
   void saveTourOperators(const vector<tour_operator> & tour_ops);
   int getNewTourOperatorId();

   // courses
   vector<vacation> loadVacations();
   void saveVacations(const vector<vacation> & vacations);
   int getNewVacationId();
   virtual optional<User> getUserAuth(
       const string & username,
       const string & password) = 0;
   virtual vector<tour_operator> getAllUserOperators(int user_id) = 0;

   // links
   virtual vector<vacation> getAllTourOperatorsVacations(int tourop_id) = 0;
   virtual bool insertTourOperatorVacation(int tourop_id, int vacation_id) = 0;
   virtual bool removeTourOperatorVacation(int tourop_id, int vacation_id) = 0;

 public:
   explicit CsvStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
   ~CsvStorage();
};

#endif // CSVSTORAGE_H
