#ifndef AUNTEFICATION_H
#define AUNTEFICATION_H

#include <QDialog>
#include <vector>
#include <string>

namespace Ui {
class auntefication;
}

class auntefication : public QDialog
{
    Q_OBJECT

public:
    explicit auntefication(QWidget *parent = 0);
    ~auntefication();
    std::vector<std::string> login();
private slots:
    void on_buttonBox_accepted();

private:
    Ui::auntefication *ui;
};

#endif // AUNTEFICATION_H
