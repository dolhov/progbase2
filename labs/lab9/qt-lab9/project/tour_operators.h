#pragma once
#include <string>

struct tour_operator
{
    int id;
    std::string name;
    std::string city;
    std::string vacation;
    int cost;
    std::string currency;
    int user_id;
};
