#include "edit.h"
#include "ui_edit.h"

Edit::Edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Edit)
{
    ui->setupUi(this);
}

Edit::~Edit()
{
    delete ui;
}


void Edit::SetStorage(Storage * st)
{
    this->st = st;
}

void Edit::SetTourOperatorId(int id)
{
    this->tourop_id = id;
}

tour_operator Edit::data()
{
    tour_operator tourop;
    tourop.name = ui->lineEdit->text().toStdString();
    tourop.city = ui->lineEdit_2->text().toStdString();
    tourop.vacation = ui->lineEdit_3->text().toStdString();
    tourop.cost = ui->spinBox->value();
    tourop.currency = ui->lineEdit_4->text().toStdString();
    return tourop;
}


 void Edit::DisplayVacations()
 {
     ui->listWidget_2->clear();
     std::vector<vacation> vac = this->st->getAllTourOperatorsVacations(this->tourop_id);
     for (vacation & vc : vac)
     {
         QString text = "id";
         text += QString::fromStdString(std::to_string(vc.id));
         text +=" ";
         text += QString::fromStdString(vc.city);
         text += ";";

         QListWidgetItem * new_item = new QListWidgetItem(text);
         QVariant var = QVariant::fromValue(vc.id);
         new_item->setData(Qt::UserRole, var);
         ui->listWidget_2->addItem(new_item);
     }
 }

 //add vacation
void Edit::on_pushButton_clicked()
{
    vacations_list dialog(this);
    dialog.setVacations(st->getAllVacations());
    int status = dialog.exec();
    qDebug() << "status: " << status;
    if (status == 1)
    {
        optional<int> vacation_id = dialog.vacationId();
        if (vacation_id)
        {
            this->st->insertTourOperatorVacation(this->tourop_id, vacation_id.value());
        }
    }
//   int id = ui->spinBox_2->value();
//   bool checker = false;
//   std::vector<vacation> vacs = this->st->getAllVacations();
//   for (vacation vc : vacs)
//   {
//       if (vc.id == id)
//       {
//           st->insertTourOperatorVacation(this->tourop_id, vc.id);
//           checker = true;
//           break;
//       }
//   }
//   if (!checker)
//   {
//       QMessageBox::information(
//           this,
//           "Information",
//           "No vacation with such id");
//   }


   this->DisplayVacations();
}

//delete vacation
void Edit::on_pushButton_2_clicked()
{
    QList list = ui->listWidget_2->selectedItems();
    if (list.size() == 0)
    {
        QMessageBox::information(
            this,
            "Information",
            "Choose entity");
    }
    else
    {
        QMessageBox::StandardButton reply;
           reply = QMessageBox::question(
               this,
               "On delete",
               "Are you sure?",
               QMessageBox::Yes|QMessageBox::No);
           if (reply == QMessageBox::Yes)
           {

                QListWidgetItem * item = list.at(list.size() - 1);
                QVariant var = item->data(Qt::UserRole);
                this->st->removeTourOperatorVacation(this->tourop_id ,var.toInt());
                QListWidgetItem * it = ui->listWidget_2->takeItem(ui->listWidget_2->currentRow());
                delete it;
           }
    }

}

void Edit::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    vacation vac = st->getVacationById(var.toInt()).value();
    ui->label_8->setText(QString::fromStdString(std::to_string(vac.duration)));
    ui->label_9->setText(QString::fromStdString(vac.hotel_name));
}
