/********************************************************************************
** Form generated from reading UI file 'auntefication.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUNTEFICATION_H
#define UI_AUNTEFICATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_auntefication
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QDialog *auntefication)
    {
        if (auntefication->objectName().isEmpty())
            auntefication->setObjectName(QStringLiteral("auntefication"));
        auntefication->resize(276, 168);
        buttonBox = new QDialogButtonBox(auntefication);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(60, 120, 171, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        gridLayoutWidget = new QWidget(auntefication);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(70, 20, 160, 80));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        lineEdit = new QLineEdit(gridLayoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        gridLayout->addWidget(lineEdit, 0, 1, 1, 1);

        lineEdit_2 = new QLineEdit(gridLayoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit_2, 1, 1, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);


        retranslateUi(auntefication);
        QObject::connect(buttonBox, SIGNAL(accepted()), auntefication, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), auntefication, SLOT(reject()));

        QMetaObject::connectSlotsByName(auntefication);
    } // setupUi

    void retranslateUi(QDialog *auntefication)
    {
        auntefication->setWindowTitle(QApplication::translate("auntefication", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("auntefication", "Username", Q_NULLPTR));
        label_2->setText(QApplication::translate("auntefication", "Password", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class auntefication: public Ui_auntefication {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUNTEFICATION_H
