/********************************************************************************
** Form generated from reading UI file 'vacations_list.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VACATIONS_LIST_H
#define UI_VACATIONS_LIST_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>

QT_BEGIN_NAMESPACE

class Ui_vacations_list
{
public:
    QDialogButtonBox *buttonBox;
    QListWidget *listWidget;

    void setupUi(QDialog *vacations_list)
    {
        if (vacations_list->objectName().isEmpty())
            vacations_list->setObjectName(QStringLiteral("vacations_list"));
        vacations_list->resize(341, 277);
        buttonBox = new QDialogButtonBox(vacations_list);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(70, 220, 181, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        listWidget = new QListWidget(vacations_list);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(40, 20, 256, 192));

        retranslateUi(vacations_list);
        QObject::connect(buttonBox, SIGNAL(accepted()), vacations_list, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), vacations_list, SLOT(reject()));

        QMetaObject::connectSlotsByName(vacations_list);
    } // setupUi

    void retranslateUi(QDialog *vacations_list)
    {
        vacations_list->setWindowTitle(QApplication::translate("vacations_list", "Dialog", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class vacations_list: public Ui_vacations_list {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VACATIONS_LIST_H
