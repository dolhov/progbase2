#pragma once
#include <iostream>
#include "tour_operators.h"


class DynamicOperatorsArray_
{
   tour_operators    * items_;
   size_t capacity_;

public:
  DynamicOperatorsArray_(size_t size);
  ~DynamicOperatorsArray_();

  size_t size();
  bool resize(size_t newSize);

  tour_operators get(int index);
  void set(int index, tour_operators value);
};
