#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "list.h"
#include "csv.h"

bool open_file_read(std::string path, std::ifstream * fin)
{
    fin->exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try
    {
        fin->open(path);
    }
    catch(const std::ifstream::failure& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "ERROR: can't open file\n";
        return false;
    }
    return true;
}


std::string read_csv_line_from(std::ifstream & fin)
{
    std::string my_data;
    std::string super_string;
    while (std::getline(fin, my_data))
    {
        if (!my_data.empty())
        {
            super_string += my_data + '\n';
        }
        if (fin.eof())
        {
            break;
        }
    }
    return super_string;
}

bool open_file_write(std::string path, std::ofstream & fout)
{
    fout.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try
    {
        fout.open(path);
    }
    catch(const std::ifstream::failure& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "ERROR: can't open file\n";
        return false;
    }
    return true;
}

void write_into_file(std::ofstream & fout, std::string str)
{
    fout << str;
}


bool close_file_ofstream(std::ofstream & fin)
{
    fin.exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try
    {
        fin.close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "ERROR: can't close file\n";
        return false;
    }
    return true;
}

bool close_file_ifstream(std::ifstream * fin)
{
    fin->exceptions(std::ifstream::badbit | std::ifstream::failbit);
    try
    {
        fin->close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        std::cout << "ERROR: can't close file\n";
        return false;
    }
    return true;
}


List createEntityListFromTable(StringTable & csvTable)
{
    List my_list;
    tour_operators tour;
    for (int i = 1; i < csvTable.size_rows(); i++)
    {
        tour.id = std::stoi(csvTable.at(i, 0));
        tour.name = csvTable.at(i, 1);
        tour.city = csvTable.at(i, 2);
        tour.vacation = csvTable.at(i, 3);
        tour.cost = std::stoi(csvTable.at(i, 4));
        tour.currency = csvTable.at(i, 5);
        my_list.insert(my_list.size(), tour);
    }
    return my_list;
}

StringTable createTableFromEntityList(List & list)
{
    StringTable my_table(list.size() + 1, 6);
    my_table.at(0, 0) = "id";
    my_table.at(0, 1) = "name";
    my_table.at(0, 2) = "city";
    my_table.at(0, 3) = "vacation";
    my_table.at(0, 4) = "cost";
    my_table.at(0, 5) = "currency";
    //id,name,city,vacation,cost,currency
    for (int i = 0; i < list.size(); i++)
    {
        my_table.at(i + 1, 0) = std::to_string(list.get(i).id);
        my_table.at(i + 1, 1) = list.get(i).name;
        my_table.at(i + 1, 2) = list.get(i).city;
        my_table.at(i + 1, 3) = list.get(i).vacation;
        my_table.at(i + 1, 4) = std::to_string(list.get(i).cost);
        my_table.at(i + 1, 5) = list.get(i).currency;
    }
    return my_table;
}


void processEntities(List & items, std::string n)
{
    for (int i = 0; i < items.size(); i++)
    {
        if (items.get(i).vacation != n)
        {
            items.remove_at(i);
            i--;
        }
    }
}