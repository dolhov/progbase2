#include <iostream>
#include <string>
#include <fstream>
#include "list.h"
#include "file_system.h"


int main(int argc, char * argv[])
{
    std::ifstream fin;
    open_file_read(argv[1], &fin);
    std::string super_string = read_csv_line_from(fin);
    close_file_ifstream(&fin);
    StringTable my_table = Csv_parse(super_string);
    std::string my_data;
    std::cout << "New table: \n";
    for (int i = 0; i < my_table.size_rows(); i++)
    {
        for (int j = 0; j < my_table.size_columns(); j++)
        {
            my_data = my_table.at(i, j);
            std::cout << " ";
            std::cout << my_data;
        }
        std::cout << std::endl;
    }
    List my_list = createEntityListFromTable(my_table);
    std::cout << "\nNew list of entities: \n";
    std::cout << "[id] [name] [city] [cost] [currency] \n";
    for (int i = 0; i < my_list.size(); i++)
    {
        tour_operators op = my_list.get(i);
        std::cout << op.id << " " << op.name << " " << op.city << " " << op.vacation << " " << op.cost << " " << op.currency << "\n";
        //id,name,city,vacation,cost,currency
    }
    std::cout << "\n";
    std::string N;
    std::cout << "\n Enter N (name of country) to process the table: ";
    std::cin >> N;
    processEntities(my_list, N);
    std::cout << "After proccesing: \n";
    for (int i = 0; i < my_list.size(); i++)
    {
        tour_operators op = my_list.get(i);
        std::cout << op.id << " " << op.name << " " << op.city << " " << op.vacation << " " << op.cost << " " << op.currency << "\n";
    }
    //
    //
    StringTable another_table =  createTableFromEntityList(my_list);
    //
    std::cout << "\n";
    std::cout << "New table: \n";
    for (int i = 0; i < another_table.size_rows(); i++)
    {
        for (int j = 0; j < another_table.size_columns(); j++)
        {
            my_data = another_table.at(i, j);
            std::cout << " ";
            std::cout << my_data;
        }
        std::cout << std::endl;
    }
    std::cout << "\n";
    std::string another_super_string = Csv_toString(another_table);
    std::ofstream fout;
    open_file_write("out.csv", fout);
    write_into_file(fout, another_super_string);
    close_file_ofstream(fout);
}


