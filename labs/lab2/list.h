#pragma once
#include "dynamic_tour_operator_array.h"

class List
{
    DynamicOperatorsArray_ array_; // dynamic array of T elements
    size_t size_;            // number of first array items filled with list data
public:
    List();

    size_t size(); // return number of items in list

    tour_operators get(int index);                // return self->items[index]
    void set(int index, tour_operators value);    // set self->items[index]
    void insert(int index, tour_operators value); // insert, shift right
    void remove_at(int index);         // remove and shift left

    void push_back(tour_operators value); // insert back
    void remove(tour_operators value);    // remove first by value
    int index_of(tour_operators value);   // find index by value
    bool contains(tour_operators value);  // check by value
    bool empty();              // check if list has any items
    void clear();              // make list empty

};
