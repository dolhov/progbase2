#include "list.h"


List::List() : array_{10}
{
    array_;
    size_ = 0;
}

size_t List::size() 
{
    return size_;
}

tour_operators List::get(int index) 
{
    tour_operators a = array_.get(index);
    return a;
}

void List::set(int index, tour_operators value) 
{
    array_.set(index, value);
}

void List::insert(int index, tour_operators value)
{
    if (size_ == array_.size() - 1)
    {
        array_.resize(size_ * 2);
    }
    for (int i = size_ + 1; i > index; i--)
    {
        array_.set(i, array_.get(i - 1));
    }
    array_.set(index, value);
    size_ += 1;
}

void List::remove_at(int index)
{
    for (int i = index; i < size_; i++)
    {
        array_.set(i, array_.get(i + 1));
    }
    size_ -= 1;
}



void List::push_back(tour_operators value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ * 2);
    }
    array_.set(size_, value);
    size_ += 1;
}

void List::remove(tour_operators value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value.city == array_.get(i).city && value.cost == array_.get(i).cost && value.currency == array_.get(i).currency && value.id == array_.get(i).id && value.name == array_.get(i).name && value.vacation == array_.get(i).vacation )
        {
            remove_at(i);
            break;
        }
    }
}

int List::index_of(tour_operators value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value.city == array_.get(i).city && value.cost == array_.get(i).cost && value.currency == array_.get(i).currency && value.id == array_.get(i).id && value.name == array_.get(i).name && value.vacation == array_.get(i).vacation )
        {
            return i;
        }
    }
}

bool List::contains(tour_operators value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value.city == array_.get(i).city && value.cost == array_.get(i).cost && value.currency == array_.get(i).currency && value.id == array_.get(i).id && value.name == array_.get(i).name && value.vacation == array_.get(i).vacation )
        {
            return true;
        }
    }
    return false;
}

bool List::empty()
{
    if (size_ == 0)
    {
        return true;
    }
    return false;
}

void List::clear()
{
    size_ = 0;
}