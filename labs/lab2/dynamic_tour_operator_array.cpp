#include "dynamic_tour_operator_array.h"
#include "tour_operators.h"


DynamicOperatorsArray_::DynamicOperatorsArray_(size_t size)
{
    DynamicOperatorsArray_::items_ = new tour_operators[size];
    DynamicOperatorsArray_::capacity_ = size;
}

DynamicOperatorsArray_::~DynamicOperatorsArray_() 
{
    delete[] items_;
    DynamicOperatorsArray_::capacity_ = 0;
}

size_t DynamicOperatorsArray_::size()
{
    return DynamicOperatorsArray_::capacity_;
}


bool DynamicOperatorsArray_::resize(size_t newSize)
{
    tour_operators * dest = new tour_operators[newSize];
    std::copy(items_, items_ + capacity_, dest);
    delete[] items_;
    items_ = dest;
}

tour_operators DynamicOperatorsArray_::get(int index)
{
    return DynamicOperatorsArray_::items_[index];
}

void DynamicOperatorsArray_::set(int index, tour_operators value)
{
    DynamicOperatorsArray_::items_[index].id = value.id;
    DynamicOperatorsArray_::items_[index].name = value.name;
    DynamicOperatorsArray_::items_[index].city = value.city;
    DynamicOperatorsArray_::items_[index].vacation = value.vacation;
    DynamicOperatorsArray_::items_[index].cost = value.cost;
    DynamicOperatorsArray_::items_[index].currency = value.currency;
}
