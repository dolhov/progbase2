#include "string_table.h"
#include <iostream>

StringTable::StringTable(size_t rows, size_t cols)
{
    nrows_ = rows;
    ncols_ = cols;
    cells_ = new std::string*[rows];
    for (int i = 0; i < rows; i++)
    {
        cells_[i] = new std::string[cols]; 
    }
}

StringTable::~StringTable()
{
    for (int i = 0; i < nrows_; i++)
    {
        delete[] cells_[i]; 
    }
    delete[] cells_;
}

size_t StringTable::size_rows()
{
    return nrows_;
}

size_t StringTable::size_columns()
{
    return ncols_;
}

std::string& StringTable::at(int rowIndex, int colIndex)
{
    return cells_[rowIndex][colIndex];
}
