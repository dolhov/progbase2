#include "FileStorage.h"
#include "csvstorage.h"
#include "xmlstorage.h"
#include "sqlitestorage.h"
#include "cui.h"



int main()
{
    //CsvStorage storage{"../../../lab8/data/csv/"};
    //XmlStorage xml_storage{"../../data/xml/"};
    SqliteStorage sql_storage{"../../data/sql/"};
    Storage * storage_ptr = &sql_storage;
    Cui cui{storage_ptr};
    cui.show();
}
