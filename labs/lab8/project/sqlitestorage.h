#pragma once

#include <string>
#include <vector>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFileInfo>
#include <QVariant>
#include <QDebug>

#include <optional>
#include "tour_operators.h"
#include "vacation.h"
#include "storage.h"

using namespace std;

class SqliteStorage : public Storage
{
 protected:
   QSqlDatabase database_;

 public:
   SqliteStorage(const string & dir_name);

   bool isOpen() const;
   bool open();
   void close();

   // students
   vector<tour_operator> getAllTourOperators(void);
   optional<tour_operator> getTourOperatorById(int operator_id);
   bool updateTourOperator(const tour_operator &tour_op);
   bool removeTourOperator(int operator_id);
   int insertTourOperator(const tour_operator &tour_op);

   // courses
   vector<vacation> getAllVacations(void);
   optional<vacation> getVacationById(int vacation_id);
   bool updateVacation(const vacation & vacation_upd);
   bool removeVacation(int vacation_id);
   int insertVacation(const vacation &vac);
};
