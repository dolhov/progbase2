#include "cui.h"


void Cui::show()
{
    int option = 0;
    std::cout << "Choose entity:\n1)Tour Operators\n2)Vacations\n";
    std::cout << "To choose option write it's number;\n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::cin >> option;
    if (option == 1)
    {
        std::cout << "Tour Operator options:\n1)Operators Info\n2)Operator info\n3)Change field of operator\n4)Delete operator\n5)Add new operator\n6)Show options again\n7)Exit\n";
        while (1)
        {
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            std::cin >> option;
            if (option == 1)
            {
                std::cout << "Operators menu:\n";
                this->operatorsMainMenu();
            }
            else if (option == 2)
            {
                std::cout << "Enter id of operator:\n";
                int id = 0;
                std::cin >> id;
                operatorsMenu(id);
            }
            else if (option == 3)
            {
                std::cout << "Enter id of operator:\n";
                int id = 0;
                std::cin >> id;
                operatorsUpdateMenu(id);
            }
            else if (option == 4)
            {
                std::cout << "Enter id of operator:\n";
                int id = 0;
                std::cin >> id;
                operatorsDeleteMenu(id);
            }
            else if (option == 5)
            {
                operatorsCreateMenu();
                std::cout << "Operator created\n";
            }
            else if (option == 6)
            {
                std::cout << "Options:\n1)Operators Info\n2)Operator info\n3)Change field of operator\n4)Delete operator\n5)Add new operator\n6)Show options again\n7)Exit\n";
            }
            else if (option == 7)
            {
                std::cout << "End of program";
                break;
            }
            else
            {
                std::cout << "Unknown option. Enter 6 to see list of options again\n";
            }
        }
    }
    else if (option == 2)
    {
        std::cout << "Vacation options: \n1)Vacations Info\n2)Vacation info\n3)Change field of vacation\n4)Delete vacation\n5)Add new vacation\n6)Show options again\n7)Exit\n";
        while (1)
        {
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            std::cin >> option;
            if (option == 1)
            {
                std::cout << "Vacation menu:\n";
                this->vacationsMainMenu();
            }
            else if (option == 2)
            {
                std::cout << "Enter id of vacation:\n";
                int id = 0;
                std::cin >> id;
                vacationMenu(id);
            }
            else if (option == 3)
            {
                std::cout << "Enter id of vacation:\n";
                int id = 0;
                std::cin >> id;
                vacationUpdateMenu(id);

            }
            else if (option == 4)
            {
                std::cout << "Enter id of vacation:\n";
                int id = 0;
                std::cin >> id;
                vacationDeleteMenu(id);
            }
            else if (option == 5)
            {
                vacationCreateMenu();
                std::cout << "Vacation created\n";
            }
            else if (option == 6)
            {
                std::cout << "Vacation options: \n1)Vacations Info\n2)Vacation info\n3)Change field of vacation\n4)Delete vacation\n5)Add new vacation\n6)Show options again\n7)Exit\n";
            }
            else if (option == 7)
            {
                std::cout << "End of program";
                break;
            }
            else
            {
                std::cout << "Unknown option. Enter 6 to see list of options again\n";
            }
        }
    }
}

void Cui::operatorsMainMenu()
{
    Storage * stor = storage_;
    std::vector<tour_operator> ops = stor->getAllTourOperators();
    std::cout << "All tour operators: \n";
    for (tour_operator & it : ops)
    {
        std::cout << it.id << " " << it.name << " " << it.city << " " << it.vacation << " " << it.cost << " " << it.currency << "\n";
    }
}

void Cui::operatorsMenu(int entity_id)
{
    std::optional<tour_operator> op = storage_->getTourOperatorById(entity_id);
    if (op)
    {
        tour_operator tour = op.value();
        std::cout << tour.id << " " << tour.name << " " << tour.city << " " << tour.vacation << " " << tour.cost << " " << tour.currency << "\n";
    }
    else
    {
        std::cout << "Not found\n";
    }
    
}

void Cui::operatorsUpdateMenu(int entity_id)
{
    std::optional<tour_operator> op = storage_->getTourOperatorById(entity_id);
    if (op)
    {
        tour_operator tour = op.value();
        int option = 20;
        std::cout << "Chose field to change:\n1)Name\n2)City\n3)Vacation\n4)Cost\n5)Currency\n";
        std::cin >> option;
        std::string change;
        std::cout << "Write changes: " << std::endl;
        std::cin.get();
        std::getline(std::cin, change);
        if (option == 1)
        {
            tour.name = change;
            storage_->updateTourOperator(tour);
        }
        else if (option == 2)
        {
            tour.city = change;
            storage_->updateTourOperator(tour);

        }
        else if (option == 3)
        {
            tour.vacation = change;
            storage_->updateTourOperator(tour);

        }
        else if (option == 4)
        {
            tour.cost = std::stoi(change);
            storage_->updateTourOperator(tour);

        }
        else if (option == 5)
        {
            tour.currency = change;
            storage_->updateTourOperator(tour);

        }
        else
        {
            std::cout << "Unknown option";
        }
        
    }
    else
    {
        std::cout << "Not found\n";
    }
}


void Cui::operatorsDeleteMenu(int entity_id)
{
    storage_->removeTourOperator(entity_id);
}

void Cui::operatorsCreateMenu()
{
    tour_operator op;
    std::string change;
    std::cout << "Enter operator's name: \n";
    std::cin.get();
    std::getline(std::cin, change);
    op.name = change;
    std::cout << "Enter operator's city: \n";
    std::getline(std::cin, change);
    op.city = change;
    std::cout << "Enter operator's vacation: \n";
    std::getline(std::cin, change);
    op.vacation = change;
    std::cout << "Enter operator's cost: \n";
    std::getline(std::cin, change);
    op.cost = std::stoi(change);
    std::cout << "Enter operator's currency: \n";
    std::getline(std::cin, change);
    op.currency = change;
    storage_->insertTourOperator(op);
}

//vacations

void Cui::vacationsMainMenu()
{
    Storage * stor = storage_;
    std::vector<vacation> ops = stor->getAllVacations();
    std::cout << "All vacations: \n";
    for (vacation & it : ops)
    {
        std::cout << it.id << " " << it.city << " " << it.duration << " " << it.hotel_name << "\n";
    }
}
void Cui::vacationMenu(int id)
{
    std::optional<vacation> op = storage_->getVacationById(id);
    if (op.has_value())
    {
        vacation it = op.value();
        std::cout << it.id << " " << it.city << " " << it.duration << " " << it.hotel_name << "\n";
    }
    else
    {
        std::cout << "Not found\n";
    }
}

void Cui::vacationUpdateMenu(int id)
{
    std::optional<vacation> op = storage_->getVacationById(id);  
    if (op)
    {
        vacation tour = op.value();
        int option = 20;
        std::cout << "Chose field to change:\n1)City\n2)Duration\n3)Hotel Name\n";
        std::cin >> option;
        std::string change;
        std::cout << "Write changes: " << std::endl;
        std::cin.get();
        std::getline(std::cin, change);
        if (option == 1)
        {
            tour.city = change;
            storage_->updateVacation(tour);
        }
        else if (option == 2)
        {
            tour.duration = std::stoi(change);
            storage_->updateVacation(tour);
        }
        else if (option == 3)
        {
            tour.hotel_name = change;
            storage_->updateVacation(tour);
        }
        else
        {
            std::cout << "Unknown option";
        }
    }
    else
    {
        std::cout << "Not found\n";
    }
}

void Cui::vacationDeleteMenu(int entity_id)
{
    storage_->removeVacation(entity_id);
}


void Cui::vacationCreateMenu()
{
    vacation op;
    std::string change;
    std::cout << "Enter vacation's city: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::cin.get();
    std::getline(std::cin, change);
    op.city = change;
    std::cout << "Enter vacations's duration: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::getline(std::cin, change);
    op.duration = std::stoi(change);
    std::cout << "Enter vacations hotel name: \n";
    if (std::cin.fail())
    {
        // get rid of failure state
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    std::getline(std::cin, change);
    op.hotel_name = change;
    storage_->insertVacation(op);
}
