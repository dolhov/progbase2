#include "sqlitestorage.h"


SqliteStorage::SqliteStorage(const string & dir_name)
{
    this->set_directory_name(dir_name);
    database_ = QSqlDatabase::addDatabase("QSQLITE");
}


vector<tour_operator> SqliteStorage::getAllTourOperators(void)
{
    vector<tour_operator> stor;
    this->open();
    QSqlQuery query("SELECT * FROM tour_operators");
    while (query.next())
    {
       string name = query.value("name").toString().toStdString();
       int id = query.value("id").toInt();
       string city = query.value("city").toString().toStdString();
       string vacation = query.value("vacation").toString().toStdString();
       int cost = query.value("cost").toInt();
       string currency = query.value("currency").toString().toStdString();
       tour_operator tour_op;
       tour_op.id = id;
       tour_op.city = city;
       tour_op.vacation = vacation;
       tour_op.cost = cost;
       tour_op.currency = currency;
       tour_op.name = name;
       stor.push_back(tour_op);

       //qDebug() << id << " | " << QString::fromStdString(name);
    }
    return stor;
}

optional<tour_operator> SqliteStorage::getTourOperatorById(int operator_id)
{
    QSqlQuery query;
    if (!database_.isOpen())
    {
        this->open();
    }
    if (!query.prepare("SELECT * FROM tour_operators WHERE id = :id"))
    {
       qDebug() << "get person query prepare error:";// << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":id", operator_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get person query exec error:";// << query.lastError();
       // return or throw or do smth else
    }
    if (query.next()) {
        string name = query.value("name").toString().toStdString();
        int id = query.value("id").toInt();
        string city = query.value("city").toString().toStdString();
        string vacation = query.value("vacation").toString().toStdString();
        int cost = query.value("cost").toInt();
        string currency = query.value("currency").toString().toStdString();
        tour_operator tour_op;
        tour_op.id = id;
        tour_op.city = city;
        tour_op.vacation = vacation;
        tour_op.cost = cost;
        tour_op.currency = currency;
        tour_op.name = name;
       return tour_op;
    } else {
       qDebug() << " not found ";
       return nullopt;
    }
}

bool SqliteStorage::updateTourOperator(const tour_operator &tour_op)
{
    QSqlQuery query;
    if (!query.prepare("UPDATE tour_operators SET name = :name, city = :city, vacation = :vacation, cost = :cost, currency = :currency WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:";// << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":name", QString::fromStdString(tour_op.name));
    query.bindValue(":city",  QString::fromStdString(tour_op.city));
    query.bindValue(":vacation",  QString::fromStdString(tour_op.vacation));
    query.bindValue(":cost", tour_op.cost);
    query.bindValue(":currency",  QString::fromStdString(tour_op.currency));
    query.bindValue(":id", tour_op.id);
    if (!query.exec()){
        qDebug() << "updateTourOperator query exec error:";// << query.lastError();
        // return or throw or do smth else
    }
}

bool SqliteStorage::removeTourOperator(int operator_id)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("DELETE FROM tour_operators WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:"; // << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":id", operator_id);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:"; //<< query.lastError();
        // return or throw or do smth else
    }
}

int SqliteStorage::insertTourOperator(const tour_operator &tour_op)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("INSERT INTO tour_operators (name, city, vacation, cost, currency) VALUES (:name, :city, :vacation, :cost, :currency)"))
    {
       qDebug() << "addTourOperator query prepare error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":name", QString::fromStdString(tour_op.name));
    query.bindValue(":city",  QString::fromStdString(tour_op.city));
    query.bindValue(":vacation",  QString::fromStdString(tour_op.vacation));
    query.bindValue(":cost", tour_op.cost);
    query.bindValue(":currency",  QString::fromStdString(tour_op.currency));
    if (!query.exec())
    {
       qDebug() << "addPerson query exec error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    return query.lastInsertId().toInt();
}

// courses
vector<vacation> SqliteStorage::getAllVacations(void)
{
    vector<vacation> stor;
    this->open();
    QSqlQuery query("SELECT * FROM vacations");
    while (query.next())
    {

       int id = query.value("id").toInt();
       string city = query.value("city").toString().toStdString();
       int duration = query.value("duration").toInt();
       string hotel_name = query.value("hotel_name").toString().toStdString();
       vacation vac;
       vac.id = id;
       vac.city = city;
       vac.duration = duration;
       vac.hotel_name = hotel_name;
       //qDebug() << id << " | " << QString::fromStdString(city);
       stor.push_back(vac);
    }
    return stor;
}

optional<vacation> SqliteStorage::getVacationById(int vacation_id)
{
    QSqlQuery query;
    if (!database_.isOpen())
    {
        this->open();
    }
    if (!query.prepare("SELECT * FROM vacations WHERE id = :id"))
    {
       qDebug() << "get person query prepare error:";// << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":id", vacation_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get person query exec error:";// << query.lastError();
       // return or throw or do smth else
    }
    if (query.next()) {
       qDebug() << " found ";
       int id = query.value("id").toInt();
       string city = query.value("city").toString().toStdString();
       int duration = query.value("duration").toInt();
       string hotel_name = query.value("hotel_name").toString().toStdString();
       vacation vac;
       vac.id = id;
       vac.city = city;
       vac.duration = duration;
       vac.hotel_name = hotel_name;
       return vac;
    } else {
       qDebug() << " not found ";
       return nullopt;
    }

}

bool SqliteStorage::updateVacation(const vacation & vacation_upd)
{
    QSqlQuery query;
    if (!query.prepare("UPDATE vacations SET hotel_name = :hotel_name, city = :city, duration = :duration WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:";// << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":hotel_name", QString::fromStdString(vacation_upd.hotel_name));
    query.bindValue(":city",  QString::fromStdString(vacation_upd.city));
    query.bindValue(":duration", vacation_upd.duration);
    query.bindValue(":id", vacation_upd.id);
    if (!query.exec()){
        qDebug() << "updatePerson query exec error:";// << query.lastError();
        // return or throw or do smth else
    }
}

bool SqliteStorage::removeVacation(int vacation_id)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("DELETE FROM vacations WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:";// << query.lastError();
        // return or throw or do smth else
    }
    query.bindValue(":id", vacation_id);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:"; //<< query.lastError();
        // return or throw or do smth else
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertVacation(const vacation &vac)
{
    if (!database_.isOpen())
    {
        this->open();
    }
    QSqlQuery query;
    if (!query.prepare("INSERT INTO vacations (city, duration, hotel_name) VALUES (:city, :duration, :hotel_name)"))
    {
       qDebug() << "vacations query prepare error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    query.bindValue(":hotel_name", QString::fromStdString(vac.hotel_name));
    query.bindValue(":city",  QString::fromStdString(vac.city));
    query.bindValue(":duration", vac.duration);
    if (!query.exec())
    {
       qDebug() << "addPerson query exec error:";
//                << query.lastError();
       // return or throw or do smth else
    }
    return query.lastInsertId().toInt();
}

bool SqliteStorage::isOpen() const
{
    return database_.isOpen();
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(directory_name()) + "data_sqlite";
    if (!QFileInfo::exists(path))
    {
       qDebug() << "Database does not exist:" << path;
       return false;
       // return throw or do smth else
    }


    database_.setDatabaseName(path);    // set sqlite database file path
    bool connected = database_.open();  // open db connection
    if (!connected) {
       qDebug() << "open database error:";
    //<< database_.lastError();
       // return or throw or do smth else
    }
    //
    // do smth with database here
    //
  /*  database_.close();*/  // close db connection
    return true;

}


void SqliteStorage::close()
{
    if (!database_.isOpen())
    {
        database_.close();
    }
}
