#pragma once
#include "dynamic_array.h"

template <typename T>
class List
{
   DynArray<T> array_;
   size_t size_;

public:
   List() : array_{16}, size_{0} {}
   ~List() {}

   size_t size() { return size_; }

   T &operator[](int i) { return array_[i]; }
   void insert(int index, T value);
   void remove_at(int index);

   void push_back(T value);
   void remove(T value);
   int index_of(T value);
   bool contains(T value);
   bool empty() { return size_ == 0; };
   void clear() { size_ = 0; };
};


template <typename T>


void List<T>::insert(int index, T value)
{
    if (size_ == array_.size() - 1)
    {
        array_.resize(size_ * 2);
    }
    for (int i = size_ + 1; i > index; i--)
    {
        array_[i] = array_[i - 1];
    }
    array_[index] = value;
    size_ += 1;
}

template <typename T>
void List<T>::remove_at(int index)
{
    for (int i = index; i < size_; i++)
    {
        array_[i] = array_[i + 1];
    }
    size_ -= 1;
}

template <typename T>

void List<T>::push_back(T value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ * 2);
    }
    array_[size_] = value;
    size_ += 1;
}
template <typename T>
void List<T>::remove(T value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value.city == array_.get(i).city && value.cost == array_.get(i).cost && value.currency == array_.get(i).currency && value.id == array_.get(i).id && value.name == array_.get(i).name && value.vacation == array_.get(i).vacation )
        {
            remove_at(i);
            break;
        }
    }
}
template <typename T>
int List<T>::index_of(T value)
{
    for (int i = 0; i < size_; i++)
    {
        if (value.city == array_[i].city && value.cost == array_[i].cost && value.currency == array_[i].currency && value.id == array_[i].id && value.name == array_[i].name && value.vacation == array_[i].vacation )
        {
            return i;
        }
    }
}
template <typename T>
bool List<T>::contains(T value)
{
    for (int i = 0; i < size_; i++)
    {
        if (array_[i] == value)
        {
            return true;
        }
    }
    return false;
}

