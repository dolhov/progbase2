#pragma once
#include "BinTree.h"


class BSTree {
    BinTree * root_ = nullptr;
    size_t size_ = 0;  // for size() O(1)


public:
    size_t size     ();  // number of stored values
    void   insert   (tour_operators value);  // add unique
    bool   lookup   (int key);  // check for value with a key
    tour_operators search   (int key);  // get the value for a key
    int find_key_for_process(BinTree * node, std::string process, size_t depth);
    tour_operators remove   (int key);  // remove the value for a key
    void   clear    ();           // remove all values
    void print();
};

int getKey(tour_operators value);  // a function to get key from value
