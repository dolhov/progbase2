#pragma once
#include <iostream>
#include "tour_operators.h"

template <typename T>
class DynArray
{
   T *items_;
   size_t capacity_;

public:
   explicit DynArray(size_t cap)
       : capacity_{cap} { items_ = new T[cap]; }
   ~DynArray() { delete[] items_; }

   size_t size() { return capacity_; }
   T &operator[](int i) { return items_[i]; }

   void resize(size_t cap)
   {
       T *newArr = new T[cap];
       size_t size = 0;
       if (capacity_ < cap)
       {
           size = capacity_;
       }
       else
       {
           size = cap;
       }
       for (int i = 0; i < size; i++)
           newArr[i] = items_[i];
       delete[] items_;
       items_ = newArr;
       capacity_ = cap;
   }
};

