#include <iostream>
#include <string>
#include <fstream>
#include "BSTree.h"
#include "list.h"
#include "file_system.h"
#include "options.h"

int main(int argc,char * argv[])
{
    Options ops = getProgramOptions(argc, argv);
    std::cout << "Chosen options: " << ops.input_file_name << "(input file), " << ops.n_process << "(process parameter), " << ops.build_bstree << "(build tree), " << ops.output_file_name << "(output file name)" <<std::endl;
    std::ifstream fin;
    open_file_read(ops.input_file_name, &fin);
    std::string super_string = read_csv_line_from(fin);
    close_file_ifstream(&fin);
    StringTable my_table = Csv_parse(super_string);
    std::string my_data;
    std::cout << "New table: \n";
    for (int i = 0; i < my_table.size_rows(); i++)
    {
        for (int j = 0; j < my_table.size_columns(); j++)
        {
            my_data = my_table.at(i, j);
            std::cout << " ";
            std::cout << my_data;
        }
        std::cout << std::endl;
    }
    List<tour_operators> my_list = createEntityListFromTable(my_table);
    std::cout << "\nNew list of entities: \n";
    std::cout << "[id] [name] [city] [cost] [currency] \n";
    for (int i = 0; i < my_list.size(); i++)
    {
        tour_operators op = my_list[i];
        std::cout << op.id << " " << op.name << " " << op.city << " " << op.vacation << " " << op.cost << " " << op.currency << "\n";
        //id,name,city,vacation,cost,currency
    }
    BSTree tree;
    if (ops.build_bstree == true)
    {
        for (int i = 0; i < my_list.size(); i++)
        {
            tree.insert(my_list[i]);
        }
        std::cout << "\n";
        std::cout << "Tree: \n";
        tree.print();
    }
    std::cout << std::endl;
    if (ops.n_process != "")
    {
        processEntities(my_list, ops.n_process); // ops.n_process
        std::cout << "\nProccesed list of entities: \n";
        std::cout << "[id] [name] [city] [cost] [currency] \n";
        for (int i = 0; i < my_list.size(); i++)
        {
            tour_operators op = my_list[i];
            std::cout << op.id << " " << op.name << " " << op.city << " " << op.vacation << " " << op.cost << " " << op.currency << "\n";
            //id,name,city,vacation,cost,currency
        }
        if (ops.build_bstree == true)
        {
            BinTree * node = nullptr;
            int key = -1;
            while (key != 0)
            {
                key = tree.find_key_for_process(node, ops.n_process, 0);
                if (key != 0)
                {
                    tree.remove(key);
                }
            }
            std::cout << "Modified tree: \n";
            tree.print();
            std::cout << std::endl;
        }
    }
    if (ops.output_file_name != "")
    {
        StringTable another_table = createTableFromEntityList(my_list);
        std::cout << "\n Modified string table: \n";
        for (int i = 0; i < another_table.size_rows(); i++)
        {
            for (int j = 0; j < another_table.size_columns(); j++)
            {
                my_data = another_table.at(i, j);
                std::cout << " ";
                std::cout << my_data;
            }
            std::cout << std::endl;
        }
        std::ofstream fout;
        std::string another_super_string = Csv_toString(another_table);
        open_file_write(ops.output_file_name, fout);
        write_into_file(fout, another_super_string);
        close_file_ofstream(fout);
    }
    tree.clear();
}