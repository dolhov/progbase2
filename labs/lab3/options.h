#pragma once
#include <string>

struct Options
{
    std::string input_file_name;
    std::string           n_process;  // T is based on your task
    std::string output_file_name;
    bool        build_bstree;
};

Options getProgramOptions(int argc, char * argv[])
{
    Options ops;
    ops.build_bstree = false;
    std::string str[argc];
    for (int i = 0; i < argc; i++)
    {
        str[i] = argv[i];
    }
    for (int i = 1; i < argc; i++)
    {
        if (str[i][0] != '-' && str[i-1] != "-n" && str[i-1] != "-o")
        {
            ops.input_file_name = str[i];
        }
        else if (str[i - 1] == "-n")
        {
            ops.n_process = str[i];
        }
        else if (str[i - 1] == "-o")
        {
            ops.output_file_name = str[i];
        }
        else if (str[i] == "-b")
        {
            ops.build_bstree = true;
        }
    }
    return ops;
}