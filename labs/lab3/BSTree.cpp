#include "BSTree.h"
#include <iostream>


size_t BSTree::size()
{
    return size_;
}


void   BSTree::insert   (tour_operators val)
{
    BinTree * search = root_;
    size_ += 1;
    if (search == nullptr)
    {
        root_ = new BinTree;
        root_->value = val;
    }
    else
    {
        while (1)
        {
            if (search->value.id == val.id)
            {
                fprintf(stderr, "Key %i already exists in BST\n", val.id);
                abort(); // or throw
            }
            else if (val.id > search->value.id)
            {
                if (search->right != nullptr)
                {
                    search = search->right;
                }
                else
                {
                    search->right = new BinTree;
                    search->right->value = val;
                    break;
                }
            }
            else
            {
                if (search->left != nullptr)
                {
                    search = search->left;
                }
                else
                {
                    search->left = new BinTree;
                    search->left->value = val;
                    break;
                }
            }
        }
    }
}


bool   BSTree::lookup   (int key)
{
    BinTree * searcher = root_;
    while(searcher != nullptr)
    {
        if (searcher->value.id == key)
        {
            return true;
        }
        if (key > searcher->value.id)
        {
            searcher = searcher->right;
        }
        else
        {
            searcher = searcher->left;
        }
    }
    return false;
}

tour_operators BSTree::search   (int key)
{
    BinTree * searcher = root_;
    while(searcher != nullptr)
    {
        if (searcher->value.id == key)
        {
            return searcher->value;
        }
        if (key > searcher->value.id)
        {
            searcher = searcher->right;
        }
        else
        {
            searcher = searcher->left;
        }
    }
    exit(1);
}


int BSTree::find_key_for_process(BinTree * node, std::string process, size_t depth)
{
    if (node == nullptr && depth == 0) 
    {
        node = root_;
    }
   if (node == nullptr) return 0;
   if (node->value.vacation != process) {
       return node->value.id;
   }
   int key = 0;
   key = find_key_for_process(node->left, process, depth += 1);   // left
   if (key != 0) return key;
   key = find_key_for_process(node->right, process, depth += 1);  // right
   return key;
}



//
//
static void modifyTreeOnRemove(BinTree * node, BinTree * parent);

static tour_operators removeNode(BinTree * node, int key, BinTree * parent) {
   if (node == nullptr) {
       fprintf(stderr, "Key `%i` not found on removal\n", key);
       abort();
   }
   if      (key < node->value.id) return removeNode(node->left,  key, node);
   else if (key > node->value.id) return removeNode(node->right, key, node);
   else  /* key == node->key*/ {
       modifyTreeOnRemove(node, parent);
       tour_operators removedValue = node->value;
       delete node; 
       return removedValue;
   }
}

static BinTree * getRemoveReplacementNode(BinTree * node);

static void modifyTreeOnRemove(BinTree * node, BinTree * parent) {
   BinTree * replacementNode = getRemoveReplacementNode(node);
   if (parent->left == node) parent->left  = replacementNode;  // find node’s position as parent’s child
   else                      parent->right = replacementNode;
}

static BinTree * modifyTreeOnCaseC(BinTree * node);

static BinTree * getRemoveReplacementNode(BinTree * node) {
   if (node->left == nullptr && node->right == nullptr) {          // case A: no children
	 return nullptr;  // nothing to replace with
   } else if (node->left == nullptr || node->right == nullptr) {   // case B: one child
       BinTree * child = (node->left != nullptr) ? node->left : node->right;  // get a pointer to child
       return child;  // replace with it’s only child
   } else /* node->left != NULL && node->right != NULL */ {  // case C: two children
       return modifyTreeOnCaseC(node);
   }
}

static BinTree * searchMin(BinTree * node);

static BinTree * modifyTreeOnCaseC(BinTree * node) {      
   BinTree * minNode = searchMin(node->right);  // find node with min value in right subtree
   int minKey = minNode->value.id;
   tour_operators removedValue = removeNode(node->right, minKey, node);  // remove min node by key
   BinTree * newMin = new BinTree; // minNode is freed in remove()
   newMin->value = removedValue; 
   newMin->left = node->left;
   newMin->right = node->right;
   return newMin;  // replace with the min node removed from it’s right subtree
}

static BinTree * searchMin(BinTree * node) {
   if (node == nullptr)       return nullptr;
   if (node->left == nullptr) return node;
   return searchMin(node->left);
}
//
//

tour_operators BSTree::remove(int key) {
   BinTree fakeParent;
   fakeParent.value.id = -1;  // to handle special root case
   fakeParent.left = this->root_;
   tour_operators old = removeNode(this->root_, key, &fakeParent);  // enter recursion
   this->root_ = fakeParent.left;
   return old;
}


void deleteTree(BinTree* node)  
{  
    if (node == NULL) return;  
  
    /* first delete both subtrees */
    deleteTree(node->left);  
    deleteTree(node->right);  
  
    /* then delete the node */
    delete node;  
}  
 
void BSTree::clear()
{
    deleteTree(this->root_);  
    root_ = nullptr;  
}

static void printValueOnLevel(BinTree * node, char pos, int depth)
{
   for (int i = 0; i < depth; i++) {
       printf("....");
   }
   printf("%c: ", pos);

   if (node == nullptr) {
       printf("(null)\n");
   } else {
       printf("%i\n", node->value.id);
   }
}

static void printNode(BinTree * node, char pos, int depth);

void printBinTree(BinTree * root) { printNode(root, '+', 0); }

static void printNode(BinTree * node, char pos, int depth)
{
   bool hasChild = node != nullptr 
                       && (node->left != nullptr || node->right != nullptr);
   printValueOnLevel(node, pos, depth);
   if (hasChild) printNode(node->left,  'L', depth + 1);
   if (hasChild) printNode(node->right, 'R', depth + 1);
}


void BSTree::print()
{
    printBinTree(this->root_);
}

int getKey(tour_operators value)
{
    return value.id;
}


//
//




