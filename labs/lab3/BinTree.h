#pragma once
#include "tour_operators.h"

struct BinTree 
{
   tour_operators value;      // set in ctor
   BinTree * left = nullptr;  
   BinTree * right = nullptr; 
};